# Projet ATL Java "Money or Death"
![](Money_or_Death/resources/images/menu.jpg)

## Introduction

Dans le cadre de ce cours, notre projet consistait à établir un jeu de labyrinthe contenant des éléments tels que des ennemis, 
des pièces, des diamants, avec une possibilité au joueur de pouvoir créer ses propres niveaux. 
Nous avons choisi ce projet, car il s’agit d’un jeu qui n’existait pas jusqu’à présent. Le nom de jeu est Money Or Death. 
Nous avons donc décidé de le développer dans le cadre de ce cours. 
Pour cela, nous avons dû utiliser certains concepts de programmation vue au cours tels que : 

- Le pattern Observateur
- Le pattern Singleton
- Le pattern DAO 
- Modèle-Vue-Contrôleur (MVC)

Nous détaillerons dans la suite la description et fonctionnement du jeu.

## But du jeu
Le but du jeu est de pouvoir récolter toutes les pièces et/ou diamants du niveau sans vous faire tuer par la meute de zombies. 
Sans les pièces et/ou diamants , vous ne passerez pas au niveau suivant ou ne finiriez pas le jeu. 
Mais il faut aussi avoir le score le plus élevé. Pour ce faire , vous devez vous battre contre les zombies. 

## Mode d'emploi

### Menu Principal
![](Money_or_Death/resources/images/documentation/buttons.png)

Lors de l’exécution de la classe **« Main.java »**. Une fenêtre avec deux boutons s’affiche.

Le premier bouton **« Choose to World to Play »** vous permettra de sélectionner un niveau en parcourant le répertoire du projet, 
un niveau par défaut ou un niveau crée par vos soins.
Le deuxième bouton **« Create World »** est un éditeur de niveau.

Avec le premier bouton sélectionnez votre niveau. Le fichier **« niveau.txt »** est le fichier par défaut , il contient deux niveaux. 

### Editeur de niveau
![](Money_or_Death/resources/images/documentation/generateur.png)

Intéressons-nous à ce cas précis. 
Un tableau de grille vous est présenté représentant la grille de jeu, celle-ci est modifiable.
Au coin supérieur droit de la fenêtre se trouve les différents composants d’un niveau (boutons des éléments).
Au coin inférieur droit vous trouverez les boutons suivants : 

- Le bouton **« Erase all »** supprime le contenu de la grille.
- Le bouton **« Save »** permet d'enregistrer un niveau déjà créé à l'aide du bouton « Save As».
- Le bouton **« Save As »** permet de creer un nouveau fichier avec le contenu de la grille.
- Le bouton **« Cancel »** permet de retourner au menu principal du jeu.

#### Creation d'un niveau
Cliquez sur le composant désiré et sélectionnez la ou les cases à affecter. 
La souris vous aidera à selectionner ou gliser sur le cases à remplir avec le composant choisi (utilisez **clic gauche**). 
Si vous avez mal placé un composant, vous pouvez le supprimer avec le **clic droit** de la souris.
Après avoir créé votre niveau, sauvegardez-le avec le bouton **« Save As »** et nommez-le.
Vous pouvez également ajouter des autres niveaux sur le fichier courant et les sauvegarder avec le bouton « Save ».

Afin de pouvoir jouer, assurez-vous que le joueur soit present dans le jeu et qu'au moins une piece et au moins un zombie est present dans le niveau.

### Règles du jeu
![](Money_or_Death/resources/images/documentation/game.png)

Les zombies raffolent des diamants en plus de votre cerveau. Ces derniers peuvent absorber les diamants et vous infligez des dégâts. 
C’est embêtant pour vous, car vous ne pourriez pas finir le niveau. 
Le joueur à la faculté spéciale d’être invisible avec la touche **« space »** en restant immobile pendant quelques secondes. 
Si le zombie vous  traverse,  vous le déroberez son diamant, mais si vous le faites trop tard , il vous attaquera même invisible et vous devrez engagez le combat. 

La touche **« A »** vous permet de donner un coup de poing de dégâts assez faible et la touche **« Z »** vous permet d’affliger un coup de pied dévastateur. 
Comme dit plus haut, les combats influent sur votre score. Un score qui sera sauvegardé à la fin de la partie (que vous gagnez ou mourrez) . 
Vous devrez introduire un pseudo à trois lettres vous représentant. Si vous faites une meilleure partie, 
vous pouvez mettre à jour votre score en réintroduisant votre ancien pseudo.

La touche **escape** vous permettra de quitter la partie courant et recommencer le jeu selon votre choix.

## Outils utilisés lors de la creation du projet
Pour tout ce qui est vue du projet, nous avons utilisé la librairie graphique **Java FX** ainsi que l’outil **Scene Builder** (FXML) pour les interfaces graphiques. 
La libraire **JavaDB Driver** a été utilisée pour une base de données embarquée afin de pouvoir stocker les statistiques du joueur (score, pseudo, nombre de pièces et de diamants). 
Pour nous répartir les tâches, nous avons utilisé l’outil en ligne [**Trello**](http://trello.com). 
Ainsi que **GitLab** pour pouvoir se synchroniser les  fichiers entre nous.

## Éléments du jeu
Chaque niveau est composé des éléments suivants : 
- Un joueur capable d'attaquer l'enemi.
- Un labyrinthe composé de murs et des échelles.
- Une ou plusieurs pièces des monnaies.
- Un enemi en particulier (Zombie).
- Un ou plusieurs diamants.

## Distribution du travail
Durant ce travail nous étions 3 développeurs (Antar Gaby, Madrane Sofiane et Ordonez Jonathan) à travailler sur ce projet. 
Nous avons réparti le travail par classes, sauf certaines classes où il a fallu les réalisées avec l’ensemble du groupe. 

Nous nous sommes forcés à travailler en même temps ainsi pour avoir des avis des autres membres du groupe ou alors pour tout simplement nous aider les uns les autres

### Repartition des taches

On s’est divisé le travail pour commencer les classes mais nous n’avons pas fait chacun une classe complètement. 
Cependant, certaines personnes se sont plus penchées sur certaines classes que d’autres, d’ou les "x" dans le tableau.

On a travaillé de manière conjointe que ce soit pour la programmation ou pour la construction de l’interface de l’application.

|                       |  Antar   | Madrane | Ordoñez |
| --------------------- |:--------:|:-------:|:-------:|
| Gestion des combats |   | X^1 |   |
| Base des données |   | X | X |
| Animation player | X | X |   |
| Animation enemy | X | X |   |
| Gestion fenêtres |   | X  | X |
| Deplacement du character | X | X |   |
| Gestion des exceptions | X | X | X |
| Design graphique Menu | X | X | X |
| Design graphique Générateur |   |   | X |
| Design graphique Jeu | X | X | X |
| Gestion des collisions | X | X |   |
| Lecture du fichier | X | X | X |
| Sauvegarde du fichier | X | X | X |
| Creation Fichiers FXML | X |   | X |
| Software design pattern MVC | X | X | X |
| Design Pattern Observer | X | X |   |
| Design Pattern Singletion | | X |   |
| Design Pattern DAO        | | X | X |
| Gestion du son | X | X | X  |
| Ajout des beans | X^2 | X^3 | X^4 |
| Gestion de l'affichage cadré sur le joueur | X |   |   |
| Gestion des threads | X | X |   |
| Control du jeu (les touches) | X | X |   |
| Affichage du mode console | X |   |   |
| Documentation Javadoc | X | X | X |
| Rapport | X | X | X |

(1) Arme, type arme, dégâts infligé.

(2) CaseView, PlayerView

(3) StatView, EnemyView

(4) Cell, ChooseButton

## Conclusion

Les objectifs attendus ont été atteints malgré toutes les difficultés rencontrées.
Ce projet nous a permis de nous familiariser avec certains concepts de programmation vue aux cours.
Durant ce travail, nous avons pris en comptes les compétences et les limites de chacun pour réaliser notre projet au mieux.

## Fabriqué avec

* [Java](https://www.java.com) - Java 1.8
* [JavaFX](https://openjfx.io/) - JavaFX 1.8
* [JavaFXSceneBuilder](https://www.oracle.com/java/technologies/javafx-scene-builder-source-code.html) 
* [IntelliJ](https://www.jetbrains.com/idea/)  
* [Trello](https://trello.com/) 
* [StarUML](http://staruml.io/) 

