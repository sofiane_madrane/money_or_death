package be.he2b.moneyordeath.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ordoñez Jonathan
 */
public class PositionTest {
    
    public PositionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of getRow method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testGetRow() {
        System.out.println("getRow");
        Position instance = null;
        int expResult = 0;
        int result = instance.getRow();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getColumn method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testGetColumn() {
        System.out.println("getColumn");
        Position instance = null;
        int expResult = 0;
        int result = instance.getColumn();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of left method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testLeft() {
        System.out.println("left");
        Position instance = null;
        Position expResult = null;
        Position result = instance.left();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of right method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testRight() {
        System.out.println("right");
        Position instance = null;
        Position expResult = null;
        Position result = instance.right();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of up method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testUp() {
        System.out.println("up");
        Position instance = null;
        Position expResult = null;
        Position result = instance.up();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of down method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testDown() {
        System.out.println("down");
        Position instance = null;
        Position expResult = null;
        Position result = instance.down();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of move method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testMove() {
        System.out.println("move");
        Direction direction = null;
        Position instance = null;
        Position expResult = null;
        Position result = instance.move(direction);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class Position.
     */
    @Test(expected = NullPointerException.class)
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Position instance = null;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    /**
     * Test 1 of position in the grid.
     */
    @Test//le mouvement est correct, Droite.
    public void gridPosition_move_cas_01(){
    	Position unePosition = new Position(4,1);
	Position positionAttendu = new Position(4,2);
	
	unePosition = unePosition.move(Direction.RIGHT);
	assertEquals(positionAttendu.toString(),unePosition.toString());
    }
    
    /**
     * Test 2 of position in the grid.
     */
    @Test//le mouvement est correct, Gauche.
    public void gridPosition_move_cas_02(){
    	Position unePosition = new Position(4,1);
	Position positionAttendu = new Position(4,0);
	
	unePosition = unePosition.move(Direction.LEFT);
	assertEquals(positionAttendu.toString(),unePosition.toString());
    }
    
    /**
     * Test 3 of position in the grid.
     */
    @Test//le mouvement est correct, Haut.
    public void gridPosition_move_cas_03(){
    	Position unePosition = new Position(2,3);
	Position positionAttendu = new Position(1,3);
	
	unePosition = unePosition.move(Direction.UP);
	assertEquals(positionAttendu.toString(),unePosition.toString());
    }
    
    /**
     * Test 4 of position in the grid.
     */
    @Test//le mouvement est correct, Bas.
    public void gridPosition_move_cas_04(){
    	Position unePosition = new Position(5,1);
	Position positionAttendu = new Position(6,1);
	
	unePosition = unePosition.move(Direction.DOWN);
	assertEquals(positionAttendu.toString(),unePosition.toString());
    }
}
