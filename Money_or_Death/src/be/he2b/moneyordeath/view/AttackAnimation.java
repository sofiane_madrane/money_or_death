package be.he2b.moneyordeath.view;


import be.he2b.moneyordeath.controller.Controller;
import be.he2b.moneyordeath.model.Player;
import be.he2b.moneyordeath.model.TypeWeapons;
import javafx.scene.image.Image;


/**
 * Represent in the view, the animate attack from player
 * Its a thread independent
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class AttackAnimation extends Thread {
    
    PlayerView viewPlayer;
    Player player;
    
    /**
     * Create this animate
     * @param view
     * @param player 
     */
    public AttackAnimation(PlayerView view, Player player){
        this.viewPlayer = view;
        this.player =  player;
    }
    
    /**
     * Performs image update processing
     */
    @Override
    public void run() {
        TypeWeapons weapons  = player.getWeaponUsed().getTypeWeapons();
        String path ; 
                
        switch(weapons) {
            case KICK :
                path = "/images/player/kick.png";
                break;
                
            case PUNCH :
                path = "/images/player/punch.png";
                break;
                
            default :
                path = "/images/player/kick.png";
                break;
        }
        viewPlayer.getIv().setImage(new Image(Controller.class.getResourceAsStream(path)));
        try {
            sleep(300);
        } catch (InterruptedException ex) {
            //           Catch au cas ou mais ne fait rien.
        }
        //Todo remettre l'image par defaut aprs celle du cout
        synchronized(this){
            player.setAttack(false);
            viewPlayer.getIv().setImage(viewPlayer.getState()[0]);
        }
    }
}
