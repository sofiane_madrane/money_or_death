package be.he2b.moneyordeath.view;

import be.he2b.moneyordeath.model.Direction;
import be.he2b.moneyordeath.model.Enemy;
import javafx.scene.image.Image;

/**
 * This class is a case in the element board. It represents the enemy's case.
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class EnemyView extends CaseView {

    private final Image[] imgsRun;
    private Image imgDead;
    private final Enemy enemy;

    /**
     * Creates the CaseView of the enemy.
     *
     * @param enemy A enemy
     */
    public EnemyView(Enemy enemy) {
        initParamEnemy(enemy.getType(), enemy.getPosGrid());
        this.enemy = enemy;
        imgsRun = new Image[24];

        initImgs();
        initCurrentImgs();
    }

    private void initCurrentImgs() { // initialise l'image courante du joueur
        getIv().setImage(imgsRun[0]);
        getIv().setImage(imgsRun[15 % imgsRun.length]);
        getIv().setFitHeight(100);
        getIv().setFitWidth(80);
        getIv().setTranslateY(getIv().getTranslateY() - 45);
        getIv().setSmooth(true);
        getIv().setCache(true);
    }

    private void initImgs() { // initiale le tableau d'images de l'ennemi
        for (int i = 0; i < 24; i++) {
            if (i < imgsRun.length) {
                String path = "/images/enemy/" + i + ".gif";
                imgsRun[i] = new Image(EnemyView.class.getResourceAsStream(path));

            }
        }
        imgDead = new Image(EnemyView.class.getResourceAsStream("/images/enemy/dead.png"));
    }

    /**
     * Updates the progresse Bar of the enemy.
     */
    public void updatePBar() {
        double val = ((double) enemy.getHealth()) * 0.01;
        pb.setProgress(val);
    }

    /**
     * Change the sense of the enemy image depending on the direction.
     */
    public void changeDirection() {
        Direction d = enemy.getDirection();
        if (d == Direction.RIGHT) {
            getIv().setScaleX(+1.0);
        } else {
            getIv().setScaleX(-1.0);
        }
    }

    /**
     * Returns the picture board when the player does move.
     *
     * @return A picture board
     */
    public Image[] getImgsRun() {
        return imgsRun;
    }

    /**
     * Checks whether if the enemy is live.
     *
     * @return true if the enemy is live otherwise false
     */
    public boolean getIsLive() {
        return enemy.getIsLive();
    }

    /**
     * Checks whether if the enemy moves.
     *
     * @return true if the enemy moves otherwise false
     */
    public boolean checkPositionMV() {
        return enemy.getPosGrid().equals(onPosition().get());
    }

    /**
     * Updates the position of the enemy.
     */
    public void updatePosition() {
        setPosition(enemy.getPosGrid());
    }

    /**
     * Returns the enemy.
     *
     * @return the enemy
     */
    public Enemy getEnemy() {
        return enemy;
    }

    public void initImageOfNotLive() {
        getIv().setImage(imgDead);
        getIv().setTranslateY(-25);
    }
}
