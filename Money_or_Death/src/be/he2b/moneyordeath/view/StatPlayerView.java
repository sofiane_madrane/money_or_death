package be.he2b.moneyordeath.view;

import be.he2b.moneyordeath.model.Player;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import java.io.Serializable;

import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * Represent a statistic player bean And this class contains inner class who
 * represent a picture with some dimension
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class StatPlayerView implements Serializable{

    private final HBox stat;
    private final RectPicture pictures[];
    private final Label txt[];
    private final ObjectProperty<Player> statPlayer;

    /**
     * Create this bean
     */
    public StatPlayerView() {
        pictures = new RectPicture[3];
        txt = new Label[5];
        statPlayer = new ObjectPropertyBase<Player>() {
            @Override
            public Object getBean() {
                return this;
            }

            @Override
            public String getName() {
                return "Player";
            }
        };

        // Init
        pictures[0] = new RectPicture("/images/heart.png");
        pictures[1] = new RectPicture("/images/diamant.png");
        pictures[2] = new RectPicture("/images/Gold.png");

        for (int i = 0; i < txt.length; i++) {
            if (i == 3) {
                txt[i] = new Label(" Score : ");
            } else {
                txt[i] = new Label("...");
            }
            txt[i].setTextFill(Color.WHITE);
            txt[i].setStyle("-fx-font: 24 arial;");
        }

        //Mise dans le layout
        stat = new HBox(pictures[0], txt[0], pictures[1], txt[1],
                pictures[2], txt[2], txt[3], txt[4]);
        stat.setAlignment(Pos.CENTER);
    }

    /**
     * Allows the update the informations
     *
     * @param index index of the label update
     * @param val the new value
     */
    public final void setLabelTxt(int index, int val) {
        if (index < 0) {
            return;
        }
        txt[index].setText(Integer.toString(val));
    }

    /**
     * Return the player
     *
     * @return the player
     */
    public final Player getPlayer() {
        return statPlayer.get();
    }

    /**
     * Allow update player in the bean
     *
     * @param player the player
     */
    public final void setPlayer(Player player) {
        statPlayer.set(player);
        txt[0].setText(" : "+Integer.toString(player.getHealth())+"   ");
        txt[1].setText(" : "+Integer.toString(player.getNbOfDiamond())+"   ");
        txt[2].setText(" : "+Integer.toString(player.getPieces())+"   ");
        txt[4].setText(Integer.toString(player.getXp()));
    }

    /**
     * Return the ObjectProperty player
     *
     * @return the ObjectProperty player
     */
    public ObjectProperty<Player> playerProperty() {
        return statPlayer;
    }

    /**
     * Return a HBox contains differents informations for the bean
     *
     * @return HBox
     */
    public HBox getStatComponement() {
        return stat;
    }

    private class RectPicture extends Parent {

        private final Rectangle frame;
        private ImageView iv;
        private ImagePattern imagePattern;

        /**
         * Create a picture with some dimension
         *
         * @param path the path of the picture
         */
        public RectPicture(String path) {
            frame = new Rectangle(35, 35);
            setPicture(path);
            getChildren().add(frame);
        }

        /**
         * Insert the picture
         *
         * @param path the picture path
         */
        public final void setPicture(String path) {
            imagePattern = getImagePattern(path); //ressources/d_0.png 
            frame.setFill(imagePattern);

        }

        private final ImagePattern getImagePattern(String path) {
            return new ImagePattern(new Image(StatPlayerView.class.getResourceAsStream(path)));
        }
    }
}
