package be.he2b.moneyordeath.view;

import be.he2b.moneyordeath.model.ElementAreaType;
import be.he2b.moneyordeath.model.Position;
import javafx.animation.TranslateTransition;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 * This class is a case in the element board. It represents the player's case.
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class PlayerView extends CaseView {

    private final Image[] imgsRunning;
    private final Image[] imgsStopped;
    private final Image[] imgsInvisible;
    private final TranslateTransition translateAnim;
    private boolean stateInvisible;

    /**
     * Creates the CaseView of the player.
     *
     * @param type the type of the case
     * @param pos the position of the case
     */
    public PlayerView(ElementAreaType type, Position pos) {
        initParam(type, pos);
        stateInvisible = false;
        imgsInvisible = new Image[19];
        imgsStopped = new Image[19];
        imgsRunning = new Image[1];

        initImgs();
        initCurrentImgs();

        translateAnim = new TranslateTransition(Duration.millis(5000), getIv());
        translateAnim.play();
    }

    private void initCurrentImgs() { // initialise l'image courante du joueur
        getIv().setImage(imgsStopped[0]);
        getIv().setImage(getImgsStopped()[15 % getImgsStopped().length]);
        getIv().setFitHeight(225);
        getIv().setFitWidth(125);
        getIv().setTranslateY(getIv().getTranslateY() - 120);
        getIv().setTranslateX(getIv().getTranslateX() - 30);
        getIv().setSmooth(true);
        getIv().setCache(true);
    }

    private void initImgs() { // initiale les tableaux d'images des joueurs
        for (int i = 0; i < 1; i++) {

            if (i < imgsRunning.length) {
                int j = i + 1;
                String path = "/images/player/d (" + j + ").png";
                imgsRunning[i] = new Image(PlayerView.class.getResourceAsStream(path));
            }
        }
        for (int i = 0; i < 20; i++) {
            if (i < imgsStopped.length) {
                int j = i + 1;
                String path = "/images/player/b (" + j + ").png";
                imgsStopped[i] = new Image(PlayerView.class.getResourceAsStream(path));
                path = "/images/player/i (" + j + ").png";
                imgsInvisible[i] = new Image(PlayerView.class.getResourceAsStream(path));
            }
        }
    }

    /**
     * Returns the picture board according to the state of the player.
     *
     * @return the picture board
     */
    public Image[] getState() {
        if (stateInvisible) {
            return imgsInvisible;
        } else {
            return imgsStopped;
        }
    }

    /**
     * Returns the picture board when the player does not move.
     *
     * @return a picture board
     */
    public Image[] getImgsStopped() {
        return imgsStopped;
    }

    /**
     * Returns the picture board when the player does move.
     *
     * @return a picture board
     */
    public Image[] getImgsRunning() {
        return imgsRunning;
    }

    /**
     * Checks whether the state is invisible.
     *
     * @return true if the state is invisible otherwise false
     */
    public boolean isStateInvisible() {
        return stateInvisible;
    }

    /**
     * Allows update the invisible state.
     *
     * @param stateInvisible a boolean for change the invisible state
     */
    public void setStateInvisible(boolean stateInvisible) {
        this.stateInvisible = stateInvisible;
    }
}
