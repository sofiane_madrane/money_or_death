package be.he2b.moneyordeath.view;

import be.he2b.moneyordeath.model.ElementAreaType;
import be.he2b.moneyordeath.model.Position;
import java.io.Serializable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

/**
 * The component is the bean.
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class CaseView extends StackPane implements Serializable {

    private ImageView iv;
    private final ObjectProperty<Position> position;
    private ElementAreaType type;

    /**
     * Represents the lives of the enemy.
     */
    protected ProgressBar pb;

    /**
     * Contains the image of the enemy and the state of his life.
     */
    protected VBox vbox;

    /**
     * Creates a component.
     */
    public CaseView() {
        Rectangle border = new Rectangle(35, 35);
        border.setFill(null);
        this.position = new SimpleObjectProperty<Position>();
        setAlignment(Pos.CENTER);
        getChildren().addAll(border);
    }

    /**
     * Initializes the component.
     *
     * @param type a type
     * @param pos a position
     */
    public void initParam(ElementAreaType type, Position pos) {
        
        this.iv = new ImageView(new Image(CaseView.class.getResourceAsStream(getPath(type))));
        this.iv.setFitWidth(80);
        this.iv.setFitHeight(80);
        this.type = type;

        getChildren().add(iv);
        setPosition(pos);
    }

    /**
     * Initializes the enemy component .
     *
     * @param type a type
     * @param pos a position
     */
    public void initParamEnemy(ElementAreaType type, Position pos) {
        this.pb = new ProgressBar(1.0);
        this.pb.setTranslateY(-45);// Pour aligner la progressBar au dessus de la 
        // tete de l'ennemi
        this.pb.setPrefSize(60, 5);// Modifie la size de la progressBar

        this.iv = new ImageView(new Image(CaseView.class.getResourceAsStream(getPath(type))));
        this.iv.setFitWidth(80);
        this.iv.setFitHeight(80);
        this.type = type;
        setPosition(pos);
        this.vbox = new VBox(pb, iv);
        getChildren().add(vbox);
    }

    /**
     * Allows update the image of the component
     *
     * @param im a image
     */
    public void setIm(Image im) {
        getChildren().remove(iv);
        iv.setImage(im);
        getChildren().add(iv);
    }

    /**
     * Return the vertical box
     *
     * @return a vertical box
     */
    public VBox test() {
        return vbox;
    }

    /**
     * Return the image view of the component
     *
     * @return the image view of the component
     */
    public ImageView getIv() {
        return iv;
    }

    /**
     * Return the position of the component. The position is a property.
     *
     * @return the position of the component
     */
    public ObjectProperty<Position> onPosition() {
        return position;
    }

    /**
     * Allows update the position of the component.
     *
     * @param position a position
     */
    public void setPosition(Position position) {
        this.position.set(position);
    }

    /**
     * Return the path as a string
     *
     * @param type a type
     * @return the path
     */
    public static String getPath(ElementAreaType type) {
        String path = new String();
        switch (type) {
            case BRICK:
                path = "/images/brick.png";
                break;
            case PIECE:
                path = "/images/Gold.png";
                break;
            case LADDER:
                path = "/images/ladder.png";
                break;
            case DIAMAND:
                path = "/images/diamant.png";
                break;
        }
        return path;
    }

    /**
     * Return the type of the component.
     *
     * @return the type
     */
    public ElementAreaType getType() {
        return type;
    }
}
