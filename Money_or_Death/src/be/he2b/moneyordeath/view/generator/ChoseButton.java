package be.he2b.moneyordeath.view.generator;

import be.he2b.moneyordeath.model.ElementAreaType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Represent a option button in the map editor.
 * @author Jonathan
 */
public class ChoseButton extends Button {
    private static int nb = 0;
    private ElementAreaType type;
    private String letter;
    
    /**
     * Create un button for the map editor.
     */
    public ChoseButton(){
        Image img;
        nb++;
        if(nb > 6 ){
            nb = 1;
        }
        switch(nb){
            case 1:
                type = ElementAreaType.BRICK;
                letter = "BR";
                img = new Image("/images/brick.png", 40, 40, false, false);
                break;
            case 2:
                type = ElementAreaType.PIECE;
                letter = "PI";
                img = new Image("/images/gold.png", 40, 40, false, false);
                break;
            case 3:
                type = ElementAreaType.LADDER;
                letter = "LA";
                img = new Image("/images/ladder.png", 40, 40, false, false);
                break;
            case 4:
                type = ElementAreaType.DIAMAND;
                letter = "DI";
                img = new Image("/images/diamant.png", 40, 40, false, false);
                break;
            case 5:
                type = ElementAreaType.PLAYER;
                letter = "PL";
                img = new Image("/images/player/a (1).png", 40, 40, true, false);
                break;
            case 6:
                type = ElementAreaType.ENEMYLEFT;
                letter = "EL";
                img = new Image("/images/enemy/0.gif", 40, 40, true, false);
                break;
            default:
                type = ElementAreaType.EMPTY;
                letter = "EM";
                img = new Image("/images/empty.png", 40, 40, false, false);
                break;
        }   
        //setPrefSize(50, 50);
        this.setGraphic(new ImageView(img));
    }
    
    /**
     * Return the type of element that represents this button.
     * @return 
     */
    public ElementAreaType getType(){
        return type;
    }
    
    /**
     * Return the name that represents this button.
     * @return A String with the button name
     */
    public String getLetter(){
        return letter;
    }
    
    @Override
    public String toString() {
        return "Button : " + letter;
    }
}
