package be.he2b.moneyordeath.view.generator;

import be.he2b.moneyordeath.model.ElementAreaType;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * Represent a element in the grid.
 * @author Jonathan
 */
public class Cell extends Parent {
    
    private final ObjectProperty<ElementAreaType> VALUE;
    private final Rectangle TILE;
    private ImagePattern imagePattern;

    /**
     * Create a cell who represent a element in the grid.
     */
    public Cell() {
        VALUE = new SimpleObjectProperty<>(ElementAreaType.EMPTY);
        TILE = new Rectangle(30,30);
        TILE.setStroke(Color.GRAY);
        
        setValue(ElementAreaType.EMPTY);
        
        getChildren().add(TILE);
    }
    
    /**
     * Modify the elemnet type of the cell.
     * @param value The elements type
     */
    public final void setValue(ElementAreaType value) {
        this.VALUE.set(value);
        setPicture(VALUE.get());
    }
    
    /**
     * Return the element type of the cell.
     * @return the current element type
     */
    public final ElementAreaType getValue() {
        return VALUE.get();
    }
    
    private void setPicture(ElementAreaType num) {
        switch(VALUE.get()){
            case EMPTY:
                imagePattern = getImagePattern("/images/empty.png");
                break;
            case BRICK:
                imagePattern = getImagePattern("/images/brick.png");
                break;
            case PLAYER:
                imagePattern = getImagePattern("/images/player/a (1).png");
                break;
            case ENEMYLEFT:
                imagePattern = getImagePattern("/images/enemy/0.gif");
                break;
            case LADDER:
                imagePattern = getImagePattern("/images/ladder.png");
                break;
            case PIECE:
                imagePattern = getImagePattern("/images/gold.png");
                break;  
            case DIAMAND:
                imagePattern = getImagePattern("/images/diamant.png");
                break;  
        }
        TILE.setFill(imagePattern);           
    }
      
    private ImagePattern getImagePattern(String file){
        return new ImagePattern(new Image(file));
    }
    
    /**
     * Return the value property of this object.
     * @return the value property
     */
    public ObjectProperty<ElementAreaType> valueProperty() {
        return VALUE;
    }
}
