package be.he2b.moneyordeath.view;

import be.he2b.moneyordeath.controller.Controller;
import be.he2b.moneyordeath.model.ElementArea;
import be.he2b.moneyordeath.model.ElementAreaType;
import be.he2b.moneyordeath.model.Enemy;
import be.he2b.moneyordeath.model.Game;
import be.he2b.moneyordeath.model.MoneyOrDeathException;
import be.he2b.moneyordeath.model.Player;
import be.he2b.moneyordeath.model.Position;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * The view of the game.
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class GameView implements Observer, EventHandler<ActionEvent> {

    @FXML
    private BorderPane bpGame;
    @FXML
    private Pane paneGame;

    private Manager manager;
    private CaseView[][] board;
    private PlayerView playerView;
    private List<EnemyView> listEnemyView;
    private StatPlayerView statPlayer;
    private ImageView ivBackGround;

    private int pace15FPS;
    private int pace30FPS;

    private Controller controller = null;

    private Timeline timerInvisible, timerAuthInv;
    
    private MediaPlayer soundLose, soundPiece, soundGame;

    private Game jeux;


    /**
     * Initializes the controller class.
     *
     * @param manager the main stage
     * @param file File to read for the game
     */
    public void initialize(Manager manager, File file) {
        this.manager = manager;

        try {
            jeux = new Game(file.getAbsolutePath());
            jeux.addObserver(this);
            statPlayer = new StatPlayerView();

            timerInvisible = new Timeline(new KeyFrame(
                    Duration.seconds(2), ae -> setToVisible()));
            timerAuthInv = new Timeline(new KeyFrame(
                    Duration.seconds(5), ae -> setAuthInv()));

            board = new CaseView[12][30];

            initScene();

            Timeline timeline = new Timeline(new KeyFrame(Duration.millis(33.33), this));//pour pouvoir animé notre player à une certaine vitesse
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.play();
            
            initSound();
        } catch (MoneyOrDeathException ex) {
            exceptionDialog(ex);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Game) {
            Platform.runLater(() -> {
                refreshGame();
            });
        }
    }
    
    @Override
    public void handle(ActionEvent event) {
        int fps = pace15FPS % playerView.getState().length;
        pace30FPS++;
        pace15FPS = (pace30FPS % 2 == 0) ? pace15FPS + 1 : pace15FPS;//ralentisement 

        if (!statPlayer.getPlayer().getAttack()) { // animation du joueur 
            if (statPlayer.getPlayer().getIsLive()) {
                playerView.getIv().setImage(playerView.getState()[fps]);
            } else {
                playerView.setStateInvisible(true); //on le rend invisible quand il meurt
                playerView.getIv().setImage(playerView.getState()[fps]);
                playerView.getIv().setTranslateY(playerView.getIv().getTranslateY() - 5);
            }
            //Monte vers les cieux
        }

        for (EnemyView enemyCurrent : listEnemyView) { // animation des ennemis
            if (enemyCurrent.getIsLive()) {
                fps = pace15FPS % enemyCurrent.getImgsRun().length;
                enemyCurrent.getIv().setImage(enemyCurrent.getImgsRun()[fps]);
            } else {
                //Image meurt
                enemyCurrent.initImageOfNotLive();
            }
            fps = 0;
        }
    }
    
    /**
     * Allows to make the player visible.
     */
    public void setToVisible() {
        playerView.setStateInvisible(false);

        jeux.getCurrentPlayer().setInvisible(false);
        jeux.getCurrentPlayer().setAuthorizedInv(false);

        timerAuthInv.play();
    }

    /**
     * Authorizes player to become invisible.
     */
    public void setAuthInv() {
        jeux.getCurrentPlayer().setAuthorizedInv(true);
    }
    
    /**
     * Stop all the game connections and theards.
     */
    public void stopConnection() {
        if (controller != null) {
            try {
                soundGame.stop();
                controller.closeConnectionDB();
            } catch (MoneyOrDeathException ex) {
                exceptionDialog(ex);
            }
        }
        jeux.stopThreadEnemy();
        jeux.stopThreadFallPlayer();
    }
    
    private void initSound() {
        URL ressourceSoundLose = getClass().getResource("/sound/game-over.wav");
        URL ressourcePiece = getClass().getResource("/sound/piece.wav");
        URL ressourceGame = getClass().getResource("/sound/game.mp3");
        Media mediaLose = new Media(ressourceSoundLose.toString());
        Media mediaPiece = new Media(ressourcePiece.toString());
        Media mediaGame = new Media(ressourceGame.toString());
        soundLose = new MediaPlayer(mediaLose);
        soundPiece = new MediaPlayer(mediaPiece);
        soundGame = new MediaPlayer(mediaGame);
        soundGame.play();
        soundGame.setCycleCount(AudioClip.INDEFINITE);
    }
    
    private void initScene() {
        bpGame.setTop(statPlayer.getStatComponement());

        initCaseView();

        jeux.startThreadEnemy();
        jeux.startThreadFallPlayer();
        controller = new Controller(jeux, paneGame, playerView, timerInvisible, manager, soundGame);
    }

    private void initCaseView() {
        List<Enemy> listEnemy = jeux.getLevel().getListEnemy();
        Player player = jeux.getCurrentPlayer();

        this.statPlayer.setPlayer(player);

        paneGame.getChildren().clear();

        initBackGround();

        playerView = new PlayerView(player.getType(), new Position(player.getPosGrid()));
        listEnemyView = new ArrayList<>();

        initStaticElement(jeux.getLevel().getTabElementArea());
        paneGame.getChildren().add(playerView);
        for (Enemy ennemyCurrent : listEnemy) {
            EnemyView enemyView = new EnemyView(ennemyCurrent);
            listEnemyView.add(enemyView);
            paneGame.getChildren().add(enemyView);
        }
    }

    private void initBackGround() {
        Image image = new Image(Main.class.getResourceAsStream("/images/fond.jpg"));
        ivBackGround = new ImageView();
        ivBackGround.setImage(image);
        ivBackGround.setFitHeight(100);
        ivBackGround.setFitWidth(100);
        ivBackGround.setScaleX(+50);
        ivBackGround.setScaleY(+20);
        paneGame.getChildren().add(ivBackGround);
    }

    private void initStaticElement(ElementArea[][] tabElement) {
        Position plyrPosition = playerView.onPosition().getValue();

        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 30; j++) {
                CaseView tile = new CaseView();
                tile.initParam(tabElement[i][j].getType(), new Position(i, j));
                board[i][j] = tile;
                paneGame.getChildren().add(tile);
            }
        }
        initScreenDisplayStaticElement(plyrPosition.getRow(), plyrPosition.getColumn());
    }

    private void refreshGame() {
        String txt;
        statPlayer.setPlayer(jeux.getCurrentPlayer());
        refreshCaseView();

        if (jeux.endLevel() && !jeux.getIsOver()) {
            txt = "Félécitation vous allez passer au niveau suivant !";
            informationGame("Niveau suivant...", txt);

            try {
                jeux.nextLevel();
            } catch (MoneyOrDeathException ex) {
                exceptionDialog(ex);
            }
            initScene();
            statPlayer.setPlayer(jeux.getCurrentPlayer());

        } else if (jeux.getIsOver()) {
            if (jeux.getWin()) {
                informationGame("WINNER", "VOUS AVEZ GAGNÉ(E) !!! ");
            } else {
                soundLose.play();
                soundLose.seek(Duration.ZERO);
                
                informationGame("GAME OVER", "VOUS ÊTES MORT(E) ... ");
            }
            //INSERTION DANS LA BD
            subscribeDB();
            //REDEMANDEZ DE RECOMMENCEZ OU QUITTEZ

            confirmationGameDialog();
            soundGame.stop();
        }
    }

    private void informationGame(String title, String txt) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setGraphic(null);
        alert.setContentText(txt);
        alert.showAndWait();
    }

    private void subscribeDB() {
        TextInputDialog dialog = new TextInputDialog("nom");

        try {
            dialog.setTitle(" Insérez votre score ! ");
            dialog.setHeaderText(controller.readRow());
            dialog.setContentText("Votre nom de joueur:");
            dialog.setGraphic(null);

            // Traditional way to get the response value.
            Optional<String> result = dialog.showAndWait();

            if (result.isPresent()) {
                while ((result.get().length() > 3 || result.get().length() < 0)
                        || result.get().equals(" ") || result.get().equals("nom")
                        || result.get().equals("")) {
                    dialog.close();
                    dialog.setContentText("3 caractere requis ! ");
                    result = dialog.showAndWait();
                }
                controller.subscribeDB(result.get());
                //controller.inscriptionDB(result.get());
            }
        } catch (MoneyOrDeathException ex) {
            exceptionDialog(ex);
        }
    }

    //Fenetre qui pourrait afficher toute nos exceptions
    private void exceptionDialog(Exception ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Money or death Exception");
        alert.setHeaderText("Regardez l'exception ci-dessous.");

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("La trace d'exception était :");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    private void confirmationGameDialog() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Money or death");
        alert.setHeaderText("Voulez-vous recommencez le jeu ? ");
        alert.setContentText("Choisissez une option... ");

        ButtonType buttonTypeYes = new ButtonType("Oui");
        ButtonType buttonTypeNo = new ButtonType("Non");

        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);

        Optional<ButtonType> result = alert.showAndWait();

        try {
            if (result.get() == buttonTypeYes) {
                controller.resetGame();
                this.manager.setScene_Start();
            } else if (result.get() == buttonTypeNo) {
                controller.closeConnectionDB();
                System.exit(0);
            }
        } catch (MoneyOrDeathException ex) {
            exceptionDialog(ex);
        }
    }

    private void refreshCaseView() {
        ElementArea[][] tabElement = jeux.getLevel().getTabElementArea();
        Player player = jeux.getCurrentPlayer();
        int colPlyrView = playerView.onPosition().getValue().getColumn();
        int rowPlyrView = playerView.onPosition().getValue().getRow();

        //Trompe la position du joueur afin d'éviter que les ennemis ne 
        //s'affiche plus dans les conditions suivants :
        if (playerView.onPosition().getValue().getRow() <= 4) {
            rowPlyrView = 4;
        } else if (playerView.onPosition().getValue().getRow() > 7) {
            rowPlyrView = 7;
        }
        if (playerView.onPosition().getValue().getColumn() <= 4) {
            colPlyrView = 4;
        } else if (player.getPosGrid().getColumn() > 25) {
            colPlyrView = 25;
        }

        if (!player.getPosGrid().equals(playerView.onPosition().get())) {
            playerView.setPosition(player.getPosGrid());
        }

        refreshShowingEnemy(rowPlyrView, colPlyrView);
        refreshDisplay();
        refreshShowingPieceDiamand(tabElement);
    }

    private void refreshShowingEnemy(int rowPlyrView, int colPlyrView) {
        for (EnemyView currentEnemyView : listEnemyView) {
            currentEnemyView.updatePBar();
            if (!currentEnemyView.checkPositionMV() // si l'ennemi c'est déplacé
                    && isIntoFrameToDisplay(rowPlyrView, colPlyrView, // s'il se trouve au alentour du joueur
                            currentEnemyView.getEnemy().getPosGrid())) {
                currentEnemyView.updatePosition();
                currentEnemyView.changeDirection();
                currentEnemyView.setVisible(true);//rend visible l'image de l'enemi si elle se trouve
                // au alentour du joueur
            }
            if (!isIntoFrameToDisplay(rowPlyrView, colPlyrView,
                    currentEnemyView.getEnemy().getPosGrid())) {
                currentEnemyView.setVisible(false);//rend invisible l'image de l'enemi si elle ne se trouve
                // pas au alentour du joueur
            }
        }
    }

    private void refreshShowingPieceDiamand(ElementArea[][] tabElement) {
        int indexCurElementView;
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 30; j++) {
                indexCurElementView = paneGame.getChildren().indexOf(board[i][j]);

                if (tabElement[i][j].getType() == ElementAreaType.EMPTY
                        && this.board[i][j].getType() == ElementAreaType.PIECE) {
                    paneGame.getChildren().get(indexCurElementView).setVisible(false);
                }
                if (tabElement[i][j].getType() == ElementAreaType.EMPTY
                        && this.board[i][j].getType() == ElementAreaType.DIAMAND) {
                    paneGame.getChildren().get(indexCurElementView).setVisible(false);
                }

            }
        }

    }

    private void refreshDisplay() {
        Position posCurPlyrView = playerView.onPosition().getValue();
        int row = posCurPlyrView.getRow();
        int col = posCurPlyrView.getColumn();

        //Trompe la position du joueur afin de maintenir un affichage correct
        // dans les cas suivants : 
        if (posCurPlyrView.getRow() <= 4) {
            row = 4;
        } else if (posCurPlyrView.getRow() > 7) {
            row = 7;
        }
        if (posCurPlyrView.getColumn() <= 4) {
            col = 4;
        } else if (posCurPlyrView.getColumn() > 25) {
            col = 25;
        }

        initScreenDisplayStaticElement(row, col);
        initScreenDisplayMovableElement(); //pour ennemi & joueur 
    }

    private void initScreenDisplayStaticElement(int rowPlayer, int colPlayer) {//repositionne les elements static contenu dans la parti à afficher
        int l = 0;
        int c = 0;
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 30; j++) {
                Node curStaticElement = paneGame.getChildren().get(
                        paneGame.getChildren().indexOf(board[i][j]));
                if (isIntoFrameToDisplay(rowPlayer, colPlayer, new Position(i, j))) {
                    curStaticElement.setVisible(true);
                    curStaticElement.setTranslateX(c * 80);
                    curStaticElement.setTranslateY(l * 80);

                    c++;
                    if (c == 9) { //afin d'atteindre un affichage avec une largeur de 9 cases 
                        l++;
                        c = 0;
                    }
                } else {
                    curStaticElement.setVisible(false);
                }
            }
        }
        
        if(jeux.isCoinCollected()){
            soundPiece.play();
            soundPiece.seek(Duration.ZERO);
            controller.setCoinCollected(false);
        }
    }

    private void initScreenDisplayMovableElement() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 30; j++) {

                Node curStaticElement = paneGame.getChildren().get(
                        paneGame.getChildren().indexOf(board[i][j]));
                //Pour les ennemis
                for (EnemyView listEnemyView1 : listEnemyView) {
                    Position posCurEnemyView = listEnemyView1.getEnemy().getPosGrid();
                    if (posCurEnemyView.getColumn() == j && posCurEnemyView.getRow() == i) {
                        //assigne la position de l'element static à l'ennemi
                        listEnemyView1.setTranslateX(curStaticElement.getTranslateX());
                        listEnemyView1.setTranslateY(curStaticElement.getTranslateY());
                    }
                }
                //Pour le player
                Position posCurPlyrView = playerView.onPosition().getValue();
                if ((posCurPlyrView.getColumn() == j && posCurPlyrView.getRow() == i)) {
                    playerView.setTranslateX(curStaticElement.getTranslateX());
                    playerView.setTranslateY(curStaticElement.getTranslateY());
                }

            }
        }
    }

    private boolean isIntoFrameToDisplay(int row, int col, Position pos) {
        //Retourne vrai si pos se trouver à 4 case de distance (hauteur & largeur)
        //de la position du joueur
        return ((pos.getRow() <= row && pos.getRow() >= row - 4)
                || (pos.getRow() >= row && pos.getRow() <= row + 4))
                && ((pos.getColumn() <= col && pos.getColumn() >= col - 4)
                || (pos.getColumn() >= col && pos.getColumn() <= col + 4));
    }
}
