package be.he2b.moneyordeath.view;

import be.he2b.moneyordeath.controller.*;
import java.io.File;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.WindowEvent;

/**
 * @author Jonathan
 * @version 1.0
 *          <p>
 *          Manages the movement between start scene, game scene and generator scene
 */
public class Manager
{   
    private final Stage stage;
    
    /**
     * @param stage The scene created on start used in all JavaFX applications.
     */
    Manager(Stage stage) {
        this.stage = stage;
    }
    
    /**
     * Return the current stage , the primary stage.
     * @return Current stage
     */
    public Stage getStage() {
        return this.stage;
    }
        
    /**
     * Switch to the main window.
     */
    public void setScene_Start() {
        FXMLLoader fxmlLoader = new FXMLLoader();

        try
        {
            //load is used with setLocation but i use url in constructor.
            Parent root = fxmlLoader.load(getClass().getClassLoader().
                getResourceAsStream("be/he2b/moneyordeath/fxml/Menu.fxml"));
            
            stage.setTitle("Money or Death");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.setOnCloseRequest((WindowEvent we) -> {
                Platform.exit();
                System.exit(0);
            });
            
            MenuController menuController = fxmlLoader.<MenuController>getController();
            menuController.initialize(this);
        }
        catch (IOException e){
            System.out.println ("Erreur : " + e.getMessage());
        }
    }

    /**
     * Switch to the game window.
     *
     * @param file The file with all the levels to play
     */
    public void setScene_Game(File file){
        FXMLLoader fxmlLoader = new FXMLLoader();

        try {
            BorderPane root = (BorderPane)fxmlLoader.load(getClass().getClassLoader().
                  getResourceAsStream("be/he2b/moneyordeath/fxml/Game.fxml"));
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            
            GameView gameController = fxmlLoader.<GameView>getController();
            gameController.initialize(this,file);
            
            stage.setOnCloseRequest((WindowEvent we) -> {
                gameController.stopConnection();
                Platform.exit();
                System.exit(0);
            });
        }
        catch (Exception e) {
            Logger.getLogger(GameView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Switch to level generator (MapEditor).
     */
    public void setScene_Generator() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        
        try
        {
            //load is used with setLocation but i use url in constructor.
            Parent root = fxmlLoader.load(getClass().getClassLoader().
                getResourceAsStream("be/he2b/moneyordeath/fxml/MapEditor.fxml"));  
            Scene scene = new Scene(root);
            stage.setResizable(true);
 
            scene.addEventFilter(MouseEvent.DRAG_DETECTED , (MouseEvent mouseEvent) -> {
                scene.startFullDrag();
            });
            
            stage.setTitle("Create World");
                        
            stage.setResizable(false);
            stage.setScene(scene);
            
            MapEditorController mapController = fxmlLoader.<MapEditorController>getController();
            //10 linges et 30 colonnes.
            mapController.initialize(this,10, 30);
        }
        catch (IOException e){
            System.out.println ("Erreur : " + e.getMessage());
        }
    }  
}
