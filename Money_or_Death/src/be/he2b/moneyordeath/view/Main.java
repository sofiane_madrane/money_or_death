package be.he2b.moneyordeath.view;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;


/**
 * Main Application.
 * @author Ordoñez Jonathan
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Manager manager = new Manager(primaryStage);
        manager.setScene_Start();
        primaryStage.show();
    }

    /**
     * Entry point for the application.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
