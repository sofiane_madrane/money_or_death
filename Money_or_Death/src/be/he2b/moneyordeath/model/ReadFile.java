package be.he2b.moneyordeath.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Read the files for loading the level game
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class ReadFile {

    private String path;
    private File file;
    private ElementArea[][] tabElement;
    private Player player;
    private List<Enemy> listEnemy;
    private int nbOfpiece;
    private int nbOfDiamand;
    // private BufferedReader br2; // #A

    /**
     * Read the file.
     * @param nomFichier the name of the file
     * @param levelList the list of grids
     * @throws MoneyOrDeathException 
     */
    public ReadFile(String nomFichier, List<Grid> levelList) throws MoneyOrDeathException {
        path = new File(nomFichier).getAbsolutePath();
        file = new File(path);
        List<String> list = new ArrayList<>();

        try (BufferedReader br2 = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String ligne;
            while ((ligne = br2.readLine()) != null) {
                list.add(ligne);
            }
            createElement(levelList, list);
            br2.close(); // #A
        } catch (FileNotFoundException f) {
            throw new MoneyOrDeathException("Le fichier n'est pas trouvé !");
        } catch (IOException e) {
            throw new MoneyOrDeathException(" Exception IO ouverture du fichier. ");
        }

    }

    private void createElement(List<Grid> levelList, List<String> list) throws MoneyOrDeathException {
        tabElement = new ElementArea[Grid.getRow()][Grid.getCol()];
        player = null;
        listEnemy = new ArrayList<>();
        nbOfpiece = 0;
        nbOfDiamand = 0;

        for (int i = 0; i < list.size(); i++) {
            String line = list.get(i);
            if ((i + 1) % 12 == 0) {
                initSpaceEmpty(tabElement);
                levelList.add(new Grid(player, listEnemy, tabElement, nbOfpiece, nbOfDiamand));
                nbOfpiece = 0;
                nbOfDiamand = 0;
                tabElement = new ElementArea[Grid.getRow()][Grid.getCol()];
                listEnemy = new ArrayList<>();
            }
            initCurrentLine(line, i);
        }
    }

    private void initCurrentLine(String line, int i) throws MoneyOrDeathException {
        for (int j = 0; j < line.length(); j++) {
            try {
                ElementArea varEa;
                Enemy enemy;
                switch (line.charAt(j)) {
                    case 'J':
                        varEa = new ElementArea(ElementAreaType.EMPTY, new Position(i % 12, j));
                        player = new Player(new Position(i % 12, j));
                        break;
                    case 'H':
                        varEa = new ElementArea(ElementAreaType.LADDER, new Position(i % 12, j));
                        break;
                    case 'P':
                        varEa = new ElementArea(ElementAreaType.PIECE, new Position(i % 12, j));
                        nbOfpiece++;
                        break;
                    case 'R':
                        varEa = new ElementArea(ElementAreaType.EMPTY, new Position(i % 12, j));
                        enemy = new Enemy(ElementAreaType.ENEMYRIGHT, new Position(i % 12, j), Direction.RIGHT);
                        listEnemy.add(enemy);
                        break;
                    case 'L':
                        varEa = new ElementArea(ElementAreaType.EMPTY, new Position(i % 12, j));
                        enemy = new Enemy(ElementAreaType.ENEMYLEFT, new Position(i % 12, j), Direction.LEFT);
                        listEnemy.add(enemy);
                        break;
                    case 'D':
                        varEa = new ElementArea(ElementAreaType.DIAMAND, new Position(i % 12, j));
                        nbOfDiamand++;
                        break;
                    case '-':
                        varEa = new ElementArea(ElementAreaType.BRICK, new Position(i % 12, j));
                        break;
                    default:
                        varEa = new ElementArea(ElementAreaType.EMPTY, new Position(i % 12, j));
                }
                tabElement[i % 12][j] = varEa;
            } catch (MoneyOrDeathException ex) {
                throw new MoneyOrDeathException("Read File -> initCurrentLine \n"
                        + "Error read text file \n" + ex.getMessage());
            }
        }
    }

    private void initSpaceEmpty(ElementArea[][] tab) {
        for (int i = 0; i < Grid.getRow(); i++) {
            for (int j = 0; j < Grid.getCol(); j++) {
                if (tab[i][j] == null) {
                    tab[i][j] = new ElementArea(ElementAreaType.EMPTY, new Position(i, j));
                }
            }
        }
    }
}
