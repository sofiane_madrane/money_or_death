package be.he2b.moneyordeath.model;

import java.io.Serializable;

/**
 * Represent a position from a element of the area.
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class Position implements Serializable {

    private int column;
    private int row;

    private Position() {
    }

    /**
     * Create a position into the grid
     *
     * @param row the row of the grid
     * @param column the column of the grid
     */
    public Position(int row, int column) {

        this.column = column;
        this.row = row;
    }

    /**
     * Copy constructor
     *
     * @param pos a position
     */
    public Position(Position pos) {

        this.column = pos.column;
        this.row = pos.row;
    }

    /**
     * Return the numbers of rows
     *
     * @return the numbers of rows
     */
    public int getRow() {
        return this.row;
    }

    /**
     * Return the numbers of columns
     *
     * @return the numbers of columns
     */
    public int getColumn() {
        return this.column;
    }

    /**
     * Return the left position
     *
     * @return the left position
     */
    public Position left() {

        Position position = new Position(this.row, this.column - 1);
        return position;
    }

    /**
     * Return the right position
     *
     * @return the right position
     */
    public Position right() {

        Position position = new Position(this.row, this.column + 1);
        return position;
    }

    /**
     * Return the up position
     *
     * @return the up position
     */
    public Position up() {

        Position position = new Position(this.row - 1, this.column);
        return position;
    }

    /**
     * Return the down position
     *
     * @return the down position
     */
    public Position down() {

        Position position = new Position(this.row + 1, this.column);
        return position;
    }

    /**
     * Returns the new position in the direction considered depending on the
     * parameter of direction.
     *
     * @param direction a direction
     * @return the new direction
     */
    public Position move(Direction direction) {
        Position pos = new Position();
        if (direction == Direction.UP) {
            pos = up();
        }
        if (direction == Direction.DOWN) {
            pos = down();
        }
        if (direction == Direction.LEFT) {
            pos = left();
        }
        if (direction == Direction.RIGHT) {
            pos = right();
        }
        return pos;

    }

    /**
     * Verify if equals this position with another position
     * @param obj Another position
     * @return is equals
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Position)) {
            return false;
        }
        Position pos = (Position) obj;
        return (this.row == pos.row) && (this.column == pos.column);
    }

    @Override
    public String toString() {
        String chaine;
        chaine = "(" + row + "," + column + ")";
        return chaine;
    }

}