package be.he2b.moneyordeath.model;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * This class represents the management of the game. As well as enemies. Which
 * are creating using thread in order to set their own behavior. This behavior
 * is defined in an internal class at the end of it.
 *
 * @author Jonathan,Sofiane,Gaby
 */
public final class Game extends Observable {

    private int indexLevelCurrent;
    private Grid levelCurrent;
    private Player currentPlayer;
    private boolean fall;
    private boolean isOver;
    private boolean win;
    private final List<ThreadMovingEnemy> THREAD_ENEMY_LIST;
    private final List<Grid> LEVEL_LIST;
    private ReadFile fileLevel;
    private ThreadFallPlayer threadPlayer;
    private ModFacade db;
    private boolean coinCollected;

    /**
     * Create the game Money Or Death.
     *
     * @throws MoneyOrDeathException
     */
    public Game() throws MoneyOrDeathException {
        this("niveau.txt");
    }

    /**
     * Create the game Money Or Death.
     *
     * @param fileName A absoule path to file
     * @throws MoneyOrDeathException
     */
    public Game(String fileName) throws MoneyOrDeathException {
        //connexion à la BD
        db = new ModFacade();

        fall = false;
        isOver = false;
        win = false;
        indexLevelCurrent = 0;
        LEVEL_LIST = new ArrayList<>();
        THREAD_ENEMY_LIST = new ArrayList<>();

        fileLevel = new ReadFile(fileName, this.LEVEL_LIST);
        levelCurrent = new Grid(LEVEL_LIST.get(indexLevelCurrent));
        currentPlayer = levelCurrent.getPlayer();
        addTheadEnemy();
        coinCollected = false;
    }

    /**
     * Start all thread of list enemy
     */
    public void startThreadEnemy() {
        for (int i = 0; i < levelCurrent.getListEnemy().size(); i++) {
            if (!THREAD_ENEMY_LIST.get(i).isInterrupted()) {
                THREAD_ENEMY_LIST.get(i).start();
            }
        }
    }

    /**
     * Stop all thread of list enemy
     */
    public void stopThreadEnemy() {
        for (int i = 0; i < levelCurrent.getListEnemy().size(); i++) {

            THREAD_ENEMY_LIST.get(i).setInterruptThread(true);
        }
        THREAD_ENEMY_LIST.clear();
    }

    /**
     * Start thread fallPlayer
     */
    public void startThreadFallPlayer() {
        threadPlayer = new ThreadFallPlayer();
        threadPlayer.start();
    }

    /**
     * Stop thread fallPlayer
     */
    public void stopThreadFallPlayer() {
        threadPlayer.setInterruptThread(true);
    }

    /**
     * Getter if there is a winner.
     *
     * @return true if
     */
    public boolean getWin() {
        return this.win;
    }

    /**
     * Getter database alias
     *
     * @return database alias
     */
    public ModFacade getDataBase() {
        return this.db;
    }

    /**
     * Getter current player.
     *
     * @return The current player
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Getter the grid level.
     *
     * @return The current level
     */
    public Grid getLevel() {
        return levelCurrent;
    }

    /**
     * Getter index of the list level.
     *
     * @return An index representing a level
     */
    public int getIndexLevelCurrent() {
        return indexLevelCurrent;
    }

    /**
     * Getter if the a coin was collected.
     *
     * @return True if the player found a coin, otherwise false
     */
    public boolean isCoinCollected(){
        return coinCollected;
    }
    
    /**
     * Setter if the coin has been collected.
     *
     * @param value New value
     */
    public void setCoinCollected(boolean value){
        coinCollected = value;
    }
    
    /**
     * Setter the current level.
     *
     * @param indexLevelCurrent The new level to play
     * @throws MoneyOrDeathException if the value is negatif
     */
    public void setLevelCurrent(int indexLevelCurrent) throws MoneyOrDeathException {
        if (indexLevelCurrent < 0 && indexLevelCurrent > LEVEL_LIST.size()) {
            throw new MoneyOrDeathException(" Index du niveau courrant inférieur negatif! ");
        }

        this.indexLevelCurrent = indexLevelCurrent;
        levelCurrent = new Grid(LEVEL_LIST.get(this.indexLevelCurrent));
    }

    /**
     * Getter if the player is falling.
     *
     * @return True if the player falls
     */
    public boolean isFall() {
        return fall;
    }

    /**
     * Setter the is down
     *
     * @param fall the new value
     */
    public void setFall(boolean fall) {
        this.fall = fall;
    }

    /**
     * Getter if the game is over.
     *
     * @return True if the game is over
     */
    public boolean getIsOver() {
        return isOver;
    }

    /**
     * Getter a list with all enemies threads present at the current level of
     * the game.
     *
     * @return a list of enemy threads
     */
    public List<ThreadMovingEnemy> getThreadEnemyList() {
        return THREAD_ENEMY_LIST;
    }

    /**
     * Starts the game with a direction for the player.
     *
     * @param direction A player's direction.
     */
    public void play(Direction direction) {
        if (!isOver) {
            initMovePlayer(direction);
            initCollide();
            testIfFinish();
            setChanged();
            notifyObservers();
        }

    }

    /**
     * Verify if the level is finished.
     *
     * @return True if all coins and diamonds were collected, otherwise false
     */
    public boolean endLevel() {
        return levelCurrent.getNumberOfpiece() == 0
                && levelCurrent.getNumberOfDiamand() == 0;
    }

    /**
     * Go to next level.
     *
     * @throws MoneyOrDeathException if the player or enemy are not present
     */
    public void nextLevel() throws MoneyOrDeathException {
        try {
            coinCollected = false;
            indexLevelCurrent++;
            setLevelCurrent(indexLevelCurrent);
            currentPlayer.setInitPosition(levelCurrent.getPlayer().getPosGrid());
            currentPlayer.setPosGrid(levelCurrent.getPlayer().getPosGrid());
            currentPlayer.validDiamonds(); // Car les diamants sont perdues si on meurt
            // Ils sont validés à la fin du niveau
            currentPlayer.resetStat();
            addTheadEnemy();
        } catch (MoneyOrDeathException ex) {
            throw new MoneyOrDeathException("Game->nextLevel() \n" + ex.getMessage());
        }
    }

    /**
     * Restarts the game to start a new game.
     *
     * @throws MoneyOrDeathException if the player or enemy are not present
     */
    public void resetGame() throws MoneyOrDeathException {
        try {
            if (!win) {
                stopThreadEnemy();
                stopThreadFallPlayer();
            }
            isOver = false;
            win = false;
            setLevelCurrent(0);
            currentPlayer = levelCurrent.getPlayer();
            currentPlayer.resetStat();
            //soundGame.stop();
            addTheadEnemy();
        } catch (MoneyOrDeathException ex) {
            throw new MoneyOrDeathException("Game->resetGame() \n" + ex.getMessage());
        }
    }

    /**
     * Changes the direction of movement of an enemy.
     *
     * @param enemy An enemy
     */
    public void inverseDirection(Enemy enemy) {
        if (enemy.getSense() == Direction.RIGHT) {
            enemy.setDirection(Direction.LEFT);
            enemy.setSense(Direction.LEFT);
        } else {
            enemy.setDirection(Direction.RIGHT);
            enemy.setSense(Direction.RIGHT);
        }
    }

    /**
     * Gives order to the character to attack with a weapon proposed by the
     * player.
     *
     * @param weaponUse A player's weapon
     */
    public void playerAttack(Weapons weaponUse) {
        List<Enemy> targets = new ArrayList<>();
        int size;
        int nbDiamondEnnemy;
        int nbDiamondLevel;
        currentPlayer.setWeaponUsed(weaponUse);

        if (!currentPlayer.isInvisible()) {
            size = foundTargets(targets);
            for (int i = 0; i < size; i++) {
                nbDiamondEnnemy = targets.get(i).getNbOfDiamond();
                nbDiamondLevel = levelCurrent.getNumberOfDiamand();

                currentPlayer.attack(targets.get(i));
                if (!targets.get(i).isAlive) {
                    levelCurrent.setNumberOfDiamand(nbDiamondLevel - nbDiamondEnnemy);
                }
            }

        }
        //Lancez l'annimation du point peut frapper dans le vent
        notifyObservers();
    }

    /**
     * Gives the order to an enemy to attack.
     *
     * @param enemy An enemy
     */
    public void enemyAttack(Enemy enemy) {
        if (enemyAcceptAttack(enemy) && enemy.getIsLive()) {
            enemy.attack(currentPlayer);
            isOver = !currentPlayer.getIsLive();
            if (currentPlayer.getIsLive() == false) {
                //soundGame.stop();
                //soundLose.play();
                //soundLose.seek(Duration.ZERO);
            }
        }
    }

    private void addTheadEnemy() {
        for (int j = 0; j < levelCurrent.getListEnemy().size(); j++) {
            THREAD_ENEMY_LIST.add(new ThreadMovingEnemy(levelCurrent.getListEnemy().get(j)));
        }
    }

    private void testIfFinish() {
        if (endLevel()) {
            if (indexLevelCurrent < LEVEL_LIST.size() - 1) {
                stopThreadEnemy();
                stopThreadFallPlayer();
            } else {
                if (isOver == false) {
                    stopThreadEnemy();
                    stopThreadFallPlayer();
                }
                win = true;
                isOver = true;
                //soundGame.stop();
            }
        }
    }

    private void initMovePlayer(Direction direction) {
        if (!currentPlayer.isInvisible()) {
            Position posSup = new Position(currentPlayer.getPosGrid().getRow() - 1, currentPlayer.getPosGrid().getColumn());
            Position posInf = new Position(currentPlayer.getPosGrid().getRow() + 1, currentPlayer.getPosGrid().getColumn());
            Position posRight = new Position(currentPlayer.getPosGrid().getRow(), currentPlayer.getPosGrid().getColumn() + 1);
            Position posLeft = new Position(currentPlayer.getPosGrid().getRow(), currentPlayer.getPosGrid().getColumn() - 1);
            Position posCurr = new Position(currentPlayer.getPosGrid().getRow(), currentPlayer.getPosGrid().getColumn());
            if (fall && posInf.getRow() == 12) {
                currentPlayer.resetPosition();
            } else {
                if (levelCurrent.getElementArea(posInf).getType() == ElementAreaType.EMPTY) {
                    currentPlayer.setPosGrid(currentPlayer.getPosGrid().move(Direction.DOWN));
                } else {
                    fall = false;
                    if ((!((levelCurrent.getElementArea(posSup).getType() == ElementAreaType.EMPTY
                            && levelCurrent.getElementArea(posCurr).getType() == ElementAreaType.EMPTY && direction == Direction.UP)
                            || (levelCurrent.getElementArea(posLeft) != null
                            && levelCurrent.getElementArea(posLeft).getType() == ElementAreaType.BRICK
                            && direction == Direction.LEFT)
                            || (levelCurrent.getElementArea(posRight) != null
                            && levelCurrent.getElementArea(posRight).getType() == ElementAreaType.BRICK
                            && direction == Direction.RIGHT)
                            || (levelCurrent.getElementArea(posInf) != null
                            && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.BRICK
                            && direction == Direction.DOWN)
                            || (levelCurrent.getElementArea(posCurr).getType() == ElementAreaType.LADDER
                            && levelCurrent.getElementArea(posSup).getType() == ElementAreaType.LADDER
                            && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.LADDER
                            && levelCurrent.getElementArea(posLeft).getType() == ElementAreaType.EMPTY
                            && levelCurrent.getElementArea(posRight).getType() == ElementAreaType.EMPTY
                            && (direction == Direction.RIGHT || direction == Direction.LEFT))))
                            && levelCurrent.contains(currentPlayer.getPosGrid().move(direction))) {
                        currentPlayer.setPosGrid(currentPlayer.getPosGrid().move(direction));
                    }

                    currentPlayer.setSense(direction);

                    posInf = new Position(currentPlayer.getPosGrid().getRow() + 1, currentPlayer.getPosGrid().getColumn());
                    if (levelCurrent.getElementArea(posInf).getType() == ElementAreaType.EMPTY) {
                        fall = true;
                    }
                }
            }
        }
    }

    private void initCollideEnemy(Enemy enemy) {
        for (int i = 0; i < Grid.getRow(); i++) {
            for (int j = 0; j < Grid.getCol(); j++) {
                Position posCol = new Position(i, j);
                if (levelCurrent.getElementArea(posCol).getType() == ElementAreaType.DIAMAND
                        && levelCurrent.getElementArea(new Position(i, j)).getPosGrid().equals(enemy.getPosGrid())) {
                    enemy.setNbOfDiamond(enemy.getNbOfDiamond() + 1);
                    levelCurrent.setElementArea(posCol, new ElementArea(ElementAreaType.EMPTY, posCol));
                }
            }
        }

        if (currentPlayer.isInvisible() && enemy.getPosGrid().equals(currentPlayer.getPosGrid())) {
            currentPlayer.addDiamonds(enemy.getNbOfDiamond());
            levelCurrent.setNumberOfDiamand(levelCurrent.getNumberOfDiamand()
                    - enemy.getNbOfDiamond());
            enemy.setNbOfDiamond(0);
            testIfFinish();
        } else {
            enemyAttack(enemy);
        }
    }

    private void initCollide() {
        //synchronized(this) {
        ElementAreaType element = levelCurrent.getElementArea(currentPlayer.getPosGrid()).getType();
        Position posCol;

        if (element == ElementAreaType.PIECE) {
            posCol = new Position(currentPlayer.getPosGrid());
            currentPlayer.addPieces();
            levelCurrent.setNumberOfpiece(levelCurrent.getNumberOfpiece() - 1);
            levelCurrent.setElementArea(posCol, new ElementArea(ElementAreaType.EMPTY, posCol));
            coinCollected = true;
            //soundPiece.play();
            //soundPiece.seek(Duration.ZERO);
        } else if (element == ElementAreaType.DIAMAND) {
            posCol = new Position(currentPlayer.getPosGrid());
            currentPlayer.addDiamonds(1);
            levelCurrent.setNumberOfDiamand(levelCurrent.getNumberOfDiamand() - 1);
            levelCurrent.setElementArea(posCol, new ElementArea(ElementAreaType.EMPTY, posCol));
        }else{
            coinCollected = false;
        }
    }

    private boolean isMovePossible(Enemy enemy) {
        Position senseInf;
        Position ennemyPos = enemy.getPosGrid();
        Position posInf = new Position(ennemyPos.getRow() + 1, ennemyPos.getColumn());

        if (enemy.getSense() == Direction.RIGHT) {
            senseInf = new Position(ennemyPos.getRow() + 1, ennemyPos.getColumn() + 1);
        } else {
            senseInf = new Position(ennemyPos.getRow() + 1, ennemyPos.getColumn() - 1);
        }

        if (levelCurrent.contains(senseInf)) {
            if (levelCurrent.getElementArea(senseInf).getType() == ElementAreaType.BRICK
                    || levelCurrent.getElementArea(senseInf).getType() == ElementAreaType.LADDER
                    || (levelCurrent.getElementArea(senseInf).getType() == ElementAreaType.EMPTY
                    && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.LADDER)) {
                return true;
            }
        }

        return false;
    }

    private void movingEnemy(Enemy enemy) {
        Position enemyPos = enemy.getPosGrid();
        Position posCurr = new Position(enemyPos.getRow(), enemyPos.getColumn());
        Position posSup = new Position(enemyPos.getRow() - 1, enemyPos.getColumn());
        Position posInf = new Position(enemyPos.getRow() + 1, enemyPos.getColumn());
        Position posRight = new Position(enemyPos.getRow(), enemyPos.getColumn() + 1);
        Position posLeft = new Position(enemyPos.getRow(), enemyPos.getColumn() - 1);

        if (!enemyAcceptAttack(enemy)) {
            if ((!isMovePossible(enemy)
                    || levelCurrent.getElementArea(posRight).getType() == ElementAreaType.BRICK
                    || levelCurrent.getElementArea(posLeft).getType() == ElementAreaType.BRICK)
                    && (enemy.getDirection() == Direction.LEFT
                    || enemy.getDirection() == Direction.RIGHT)) {
                inverseDirection(enemy);
            }

            if (levelCurrent.getElementArea(posCurr).getType() == ElementAreaType.EMPTY
                    && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.LADDER
                    && enemy.getDirection() == Direction.UP) {
                enemy.setDirection(enemy.getSense());
            } else if (levelCurrent.getElementArea(posCurr).getType() == ElementAreaType.EMPTY
                    && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.LADDER
                    && enemy.getDirection() != Direction.DOWN) {
                enemy.setDirection(Direction.DOWN);
            }

            if (levelCurrent.getElementArea(posCurr).getType() == ElementAreaType.LADDER
                    && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.BRICK
                    && levelCurrent.getElementArea(posSup).getType() == ElementAreaType.LADDER
                    && enemy.getDirection() != Direction.DOWN) {
                enemy.setDirection(Direction.UP);
            } else if (levelCurrent.getElementArea(posCurr).getType() == ElementAreaType.LADDER
                    && levelCurrent.getElementArea(posInf).getType() == ElementAreaType.BRICK
                    && levelCurrent.getElementArea(posSup).getType() == ElementAreaType.LADDER
                    && enemy.getDirection() == Direction.DOWN) {
                enemy.setDirection(enemy.getSense());
            }

            enemy.setPosGrid(enemy.getPosGrid().move(enemy.getDirection()));
        }
    }

    private int foundTargets(List<Enemy> targets) {
        Enemy enemy;
        for (int i = 0; i < levelCurrent.getListEnemy().size(); i++) {
            enemy = levelCurrent.getListEnemy().get(i);
            if (playerAcceptAttack(enemy)) {
                targets.add(enemy);
            }
        }
        return targets.size();
    }

    private boolean enemyAcceptAttack(Enemy enemy) {
        int row = currentPlayer.getPosGrid().getRow();
        int col = currentPlayer.getPosGrid().getColumn();

        return ((!currentPlayer.isInvisible()) && (enemy.getSense() == Direction.LEFT
                && (currentPlayer.getSense() == Direction.RIGHT
                || currentPlayer.getSense() == Direction.LEFT)
                && enemy.getPosGrid().equals(new Position(row, col + 1)))
                || (enemy.getSense() == Direction.RIGHT
                && (currentPlayer.getSense() == Direction.RIGHT
                || currentPlayer.getSense() == Direction.LEFT)
                && enemy.getPosGrid().equals(new Position(row, col - 1))));
    }

    private boolean playerAcceptAttack(Enemy enemy) {
        int row = currentPlayer.getPosGrid().getRow();
        int col = currentPlayer.getPosGrid().getColumn();

        return ((!currentPlayer.isInvisible()) && (enemy.getSense() == Direction.LEFT
                && currentPlayer.getSense() == Direction.RIGHT
                && enemy.getPosGrid().equals(new Position(row, col + 1))
                || (enemy.getSense() == Direction.RIGHT
                && currentPlayer.getSense() == Direction.LEFT
                && enemy.getPosGrid().equals(new Position(row, col - 1)))));
    }

    private class ThreadMovingEnemy extends Thread {

        private final Enemy enemy;
        private boolean interruptThread;

        /**
         * Create thread move enemy
         *
         * @param enemy
         */
        public ThreadMovingEnemy(Enemy enemy) {
            this.enemy = enemy;
            this.interruptThread = false;
        }

        /**
         * Run move thread enemy
         */
        @Override
        public void run() {
            while (enemy.getIsLive()) {
                try {
                    if (interruptThread || isOver) {
                        this.interrupt();
                    } else {
                        initCollideEnemy(enemy);
                        movingEnemy(enemy);
                        Thread.sleep(500);
                        if (!endLevel() && !isOver) {
                            setChanged();
                            notifyObservers();
                        }
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Grid.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        /**
         * Getter if thread is interrupt
         *
         * @return thred is interrupt
         */
        public boolean isInterruptThread() {
            return interruptThread;
        }

        /**
         * Setter for interrupt thread
         *
         * @param interruptThread
         */
        public void setInterruptThread(boolean interruptThread) {
            this.interruptThread = interruptThread;
        }
    }

    private class ThreadFallPlayer extends Thread {

        private boolean interruptThread = false;
        private final int FALL_DAMAGE = 1;

        /**
         * Run thread fall
         */
        @Override
        public void run() {
            while (currentPlayer.isAlive) {
                if (interruptThread || isOver) {
                    this.interrupt();
                } else {
                    if (fall) {
                        play(Direction.DOWN);
                        currentPlayer.getInjury(FALL_DAMAGE);
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        /**
         * Getter if thread is interrupt
         *
         * @return thred is interrupt
         */
        public boolean isInterruptThread() {
            return interruptThread;
        }

        /**
         * Setter for interrupt thread
         *
         * @param interruptThread
         */
        public void setInterruptThread(boolean interruptThread) {
            this.interruptThread = interruptThread;
        }
    }
}
