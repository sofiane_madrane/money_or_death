package be.he2b.moneyordeath.model;

import java.io.Serializable;

/**
 * Represent the type element of the area
 * @author Ordoñez Jonathan
 * @author Antar Gaby
 * @author Madrane Sofiane
 */
public enum ElementAreaType implements Serializable{

    PLAYER, ENEMYRIGHT, ENEMYLEFT, BRICK, LADDER, PIECE, EMPTY , DIAMAND
}
