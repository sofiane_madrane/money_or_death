package be.he2b.moneyordeath.model;

/**
 * Represent the direction of a characters
 * @author Ordoñez Jonathan
 * @author Antar Gaby
 * @author Madrane Sofiane
 */
public enum Direction {

    UP, DOWN, RIGHT, LEFT
}
