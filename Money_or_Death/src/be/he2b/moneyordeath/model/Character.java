package be.he2b.moneyordeath.model;

/**
 * Represent a character who moves and performs some actions. 
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
abstract public class Character extends ElementArea {
    protected int health ;
    protected  final int healthMax;
    protected Direction direction ; 
    protected Direction sense ; 
    protected boolean isAlive ; 
    protected int nbOfDiamond  ; 
    protected boolean attack ;
    
    /**
     * Constructor a character
     * @param health health a character
     * @param ea her type
     * @param initPosition her initial position
     * @throws MoneyOrDeathException 
     */
    public Character(int health, ElementAreaType ea, Position initPosition) throws MoneyOrDeathException {
        super(ea, initPosition);
        this.healthMax = 100;
        this.attack = false;
        setHealth(health);
        
    }
    
    /**
     * Inflicts damage on the character
     * @param damage The damage
     */
    public abstract void getInjury(int damage) ; 
    
    /**
     * Attack another character
     * @param c Another character
     */
    public abstract void attack (Character c) ;
    
    /**
     * Getter the health of the character.
     * @return the health
     */
    public int getHealth(){
        return health ;
    }
    
    /**
     * Setter the health of the character
     * @param health the new health
     * @throws MoneyOrDeathException 
     */
    public final void setHealth(int health) throws MoneyOrDeathException{
        if (health<0) 
            throw new MoneyOrDeathException( " La vie d'un personnage est négatif " );
        if (health>healthMax) 
            throw new MoneyOrDeathException( " La vie d'un personnage depasse la limite maximun " );
        
        this.health = health ;
    }
    
     /**
     * Return the sense of movement of the enemy
     *
     * @return the sense
     */
    public Direction getSense() {
        return sense;
    }

    /**
     * Allow update the sense of the enemy
     *
     * @param sense a sense
     */
    public void setSense(Direction sense) {
        this.sense = sense;
    }

    /**
     * Return the direction of displacement of the enemy
     *
     * @return the sense
     */
    public Direction getDirection() {
        return direction;
    }
    
    /**
     * Allows update the direction
     *
     * @param direction a direction
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }
    
    /**
     * Return if ennemy is alive
     * @return alive
     */
    public boolean getIsLive() {
        return this.isAlive ; 
    }
    
    /**
     * Allows update if ennemy is dead
     *
     * @param isAlive
     */
    public void setAlive(boolean isAlive) {
        this.isAlive = isAlive ;
    }
    
    /**
     * Return the number of the diamond
     * @return the number
     */
    public int getNbOfDiamond() {
        return nbOfDiamond;
    }
    
   /**
     * Allows update the number of the diamond
     *
     * @param nbOfDiamond a direction
     */
    public void setNbOfDiamond(int nbOfDiamond) {
        this.nbOfDiamond = nbOfDiamond;
    }
    
    /**
     * Return if this character attack another character
     * @return attack another character
     */
    public boolean getAttack() {
        return this.attack;
    }
    
    /**
     * Allows update attack another character
     * @param attack attack another character
     */
    public void setAttack(boolean attack) {
        this.attack = attack;
    }
     
}
