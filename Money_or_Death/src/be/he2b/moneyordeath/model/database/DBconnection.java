package be.he2b.moneyordeath.model.database;

import be.he2b.moneyordeath.model.MoneyOrDeathException;
import be.he2b.moneyordeath.model.Player;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * This class control the database as well as its connections
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class DBconnection {
    private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String JDBC_URL = "jdbc:derby:database";
    
    Connection conn;
    
    /**
     * Create the database
     * @throws MoneyOrDeathException 
     */
    public void createDB() throws MoneyOrDeathException{
        Statement stmt = null ;
        try {
            Class.forName(DRIVER).newInstance();
            System.out.println("DRIVER OK ! ");
            
            conn = DriverManager.getConnection(JDBC_URL + ";create=true");
    
            System.out.println("Creation de table Player");
                
            stmt = conn.createStatement();
             
            //creation DDL
            stmt.executeUpdate("CREATE TABLE Player"
                                       + "(pId INT NOT NULL CONSTRAINT idPlayer PRIMARY KEY "                 
                                        +" GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                                        + "pName VARCHAR(30) NOT NULL,"
                                        + "pScore DECIMAL DEFAULT 0,"
                                        + "pDiamonds DECIMAL DEFAULT 0,"
                                        + "pPieces DECIMAL DEFAULT 0,"
                                        + "CONSTRAINT uName UNIQUE (pName))");
            
            
               
            System.out.println("Creation Reussi !");
            
        } catch (SQLException | ClassNotFoundException | 
                InstantiationException | IllegalAccessException ex) {
            throw new MoneyOrDeathException("Constructor create DB \n" + ex.getMessage());
        }finally{
            closeStatement(stmt);
        }
    }

    /**
     * Verify if the database exist
     * @return exist 
     * @throws MoneyOrDeathException 
     */
    public boolean exists() throws MoneyOrDeathException{
        boolean res = false;
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(JDBC_URL);
            res = (conn != null);
        } catch (ClassNotFoundException ex) {
            throw new MoneyOrDeathException("DB exists() ClassNotFoundException\n"
                                                + ex.getMessage());
        } catch ( SQLException ex ) {
            // Ne fais rien car si la DB n'existe pas cette exception est lancé
            // Et il faut que cette methode retourne faux pour qu'une DB soit crée.
        }
        
        return res;
    }
    
    /**
     * Delete the tuple in database
     * @param id Id player
     * @throws MoneyOrDeathException 
     */
    public void deleteStateDb(int id) throws MoneyOrDeathException {
        PreparedStatement pStmt=null;
        String query = "DELETE FROM Player "
                        + "WHERE pId=?";
     
        try {
            pStmt = conn.prepareStatement(query);
            pStmt.setInt(1,id);
            pStmt.executeUpdate();
        } catch (SQLException ex) {
           throw new MoneyOrDeathException("Joueur, suppression impossible:\n" + ex.getMessage());
        }finally{
            closeStatement(pStmt);
        }
     }
     
    /**
     * Displays tuples of the database
     * @return the tuples
     * @throws MoneyOrDeathException 
     */
    public String selectStateDb() throws MoneyOrDeathException {
        Statement stmt=null;
        String query = "SELECT * FROM Player ORDER BY pScore DESC";
        String resultTxt = "";
        String noRow = " Il y a pas de ligne à afficher. ";
        int rang = 1;

        try {
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(query);
           
            while (result.next()) {
                int id = result.getInt("pId");
                int score = result.getInt("pScore");
                String nom = result.getString("pName");
                int diamond = result.getInt("pDiamonds");
                int pieces = result.getInt("pPieces");
                resultTxt = resultTxt+rang+" Nom:"+nom+" Diamants:"+diamond
                            +" Pieces:"+pieces+" Score:"+score+"\n";               
                rang++;
            }
        } catch (SQLException ex) {
            throw new MoneyOrDeathException("affichage impossible select \n" + ex.getMessage());
        } finally {
            closeStatement(stmt);
        }  
        if (resultTxt.length()>1)
            return resultTxt ;
        else 
            return noRow ;
    }
    
    /**
     * Verify if a player is present in database
     * @param name The name player
     * @return is present in database
     * @throws MoneyOrDeathException 
     */
    public boolean existPlayer(String name) throws MoneyOrDeathException {
        PreparedStatement pStmt=null;
        String query = "SELECT * FROM Player WHERE pName=?";
        boolean ret = false ;
        try {
            pStmt = conn.prepareStatement(query);
            pStmt.setString(1, name);
            ResultSet result = pStmt.executeQuery();
            ret = result.next();
        }   catch (SQLException ex) {
             throw new MoneyOrDeathException("Joueur impossible à trouver :\n" + ex.getMessage());
        } finally{
            try {
                closeStatement(pStmt);
            } catch (MoneyOrDeathException ex) {
                 throw new MoneyOrDeathException("existPlayer closeStatement :\n" + ex.getMessage());
            }
        }

        return ret;
    }
        
    /**
     * Insert a player in database
     * @param player A player
     * @throws MoneyOrDeathException 
     */
    public void insertStateDB(Player player) throws MoneyOrDeathException{
        PreparedStatement insert = null ;
        String query = "INSERT INTO player(pName, pScore, pDiamonds,pPieces) "
                            + "VALUES(?, ?, ?, ?)" ;
        try {
            insert = conn.prepareStatement(query);
            insert.setString(1, player.getName());
            insert.setInt(2, player.getXp());
            insert.setInt(3, player.getFinalDiamonds());
            insert.setInt(4, player.getFinalPieces());
            insert.executeUpdate();
        } catch (SQLException ex) { 
            throw new MoneyOrDeathException("Insertion d'un joueur impossible:\n" + ex.getMessage());
        }finally{
             closeStatement(insert);
        }
    } 
    
    /**
     * Update the tuple player in database
     * @param player A Player
     * @throws MoneyOrDeathException 
     */
    public void updateStateDB(Player player) throws MoneyOrDeathException {
        PreparedStatement update = null; 
        //on suppose que le nom du joueur est en minuscule.
        String sql = "UPDATE Player SET "
                + "pScore=?,"
                + "pDiamonds=?, "
                + "pPieces=? "
                + "WHERE pName =?";
        try {
            update = conn.prepareStatement(sql);
            update.setInt(1, player.getXp());
            update.setInt(2, player.getFinalDiamonds());          
            update.setInt(3, player.getFinalPieces());
            update.setString(4, player.getName());
            update.executeUpdate();
        } catch (SQLException ex) {
            throw new MoneyOrDeathException("Joueur, modification impossible:\n" + ex.getMessage());
        }
    }
    
    /**
     * Close the statement in the communication with database
     * @param stmt the statement
     * @throws MoneyOrDeathException 
     */
    public void closeStatement(Statement stmt) throws MoneyOrDeathException{
        if (stmt != null) { 
            try {
                stmt.close();
            } catch (SQLException ex) {
                 throw new MoneyOrDeathException("Statement : closeStatement \n" + ex.getMessage());
            }
        }
    }
    
    /**
     * Close the connection of the database.
     * @throws MoneyOrDeathException 
     */
    public  void closeConnection() throws MoneyOrDeathException {
        if(conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                 throw new MoneyOrDeathException("connection : closeConnection \n" + ex.getMessage());
            }
        }
    }
    
}
