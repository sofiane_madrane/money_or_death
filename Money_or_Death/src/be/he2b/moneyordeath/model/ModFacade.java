package be.he2b.moneyordeath.model;

import be.he2b.moneyordeath.model.database.DBconnection;


 
/**
 * Represent the connection the model with the database
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class ModFacade {
    private DBconnection db;
    
    /**
     * Create the connection the model with the database
     * @throws MoneyOrDeathException
     */
    public ModFacade() throws MoneyOrDeathException {
        db = new DBconnection();
        try {
            if(!db.exists()){
                System.out.println("la BD exists n'exists pas");
                db.createDB();
            }else
               System.out.println("la BD exists "); 
        } catch (MoneyOrDeathException ex) {
            throw new MoneyOrDeathException("Connect the model with the database"
                    + "(ModFacade class) \n" + ex.getMessage());
        }
    }
    
    /**
     * Displays tuples of the database
     * @return the tuples
     * @throws MoneyOrDeathException 
     */
    public String readRow() throws MoneyOrDeathException {
        return  db.selectStateDb();
    }
    
    /**
     * Verify if a player is present in database
     * @param name The name player
     * @return is present in database
     * @throws MoneyOrDeathException 
     */
    public boolean existPlayer(String name) throws MoneyOrDeathException {
        return db.existPlayer(name);
    }
    
    /**
     * Insert a player in database
     * @param p A player
     * @throws MoneyOrDeathException 
     */
    public void insertPlayer(Player p) throws MoneyOrDeathException {
            db.insertStateDB(p);    
    }
    
    /**
     * Update the tuple player in database
     * @param p A Player
     * @throws MoneyOrDeathException 
     */
    public void updatePlayer(Player p) throws MoneyOrDeathException {
            db.updateStateDB(p);    
    }
    
    /**
     * Close the connection of the database.
     * @throws MoneyOrDeathException 
     */
    public void closeConnection() throws MoneyOrDeathException {      
            db.closeConnection();
    }
}
