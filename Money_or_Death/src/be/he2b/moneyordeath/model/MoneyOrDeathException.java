package be.he2b.moneyordeath.model;

/**
 * Exception thrown as soon as one asks the model part something incoherent.
 * @author Madrane Sofiane
 * @author Ordoñez Jonathan
 * @author Antar Gaby
*/
public class MoneyOrDeathException extends Exception {
    /**
    * Displays the message error
    * @param message represent exception
    */
    public MoneyOrDeathException( String  message ) {
        super(message);
    }
}
