package be.he2b.moneyordeath.model;

/**
 * Represent the player in the game. Our hero
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class Player extends Character {

    private final static ElementAreaType EA = ElementAreaType.PLAYER;
    private int xp;
    private int pieces;
    private int finalPieces;
    private int finalDiamonds;
    private boolean invisible;
    private boolean authorizedInv;
    private Weapons weaponUsed;
    private String name;
    private Position initPos;

    /**
     * Create a player.
     *
     * @param initPosition the position of the player
     * @param health the health of the player
     * @throws MoneyOrDeathException
     */
    public Player(Position initPosition, int health) throws MoneyOrDeathException {
        super(health, EA, initPosition);
        nbOfDiamond = 0;
        finalDiamonds = 0;
        pieces = 0;
        finalPieces = 0;
        invisible = false;
        authorizedInv = true;
        isAlive = true;
        xp = 0;
        initPos = new Position(getPosGrid());
        weaponUsed = new Weapons(TypeWeapons.PUNCH, 15); //Coup de poing
    }

    /**
     * Create a player. By default health is 100.
     *
     * @param initPosition the position of the player
     * @throws MoneyOrDeathException
     */
    public Player(Position initPosition) throws MoneyOrDeathException {
        this(initPosition, 100);
    }

    /**
     * Create a player. By default health is 100.
     *
     * @param name the name of the player
     * @param initPosition the position of the player
     * @throws MoneyOrDeathException
     */
    public Player(String name, Position initPosition) throws MoneyOrDeathException {
        this(initPosition, 100);
        this.name = name;
    }

    /**
     * Create a player (Copy constructor)
     *
     * @param aPlayer
     * @throws MoneyOrDeathException
     */
    public Player(Player aPlayer) throws MoneyOrDeathException {
        this(aPlayer.getPosGrid());
    }

    /**
     * Indicate whether the player is invisible or no
     *
     * @return true the player is invisible, false the player is not invisible
     */
    public boolean isInvisible() {
        return invisible;
    }

    /**
     * Allows update the invisibility of the player
     *
     * @param invisible a boolean for the invisibility
     */
    public void setInvisible(boolean invisible) {
        this.invisible = invisible;
    }

    /**
     * Allows update the health of the player
     *
     * @param damage the number of lives decreased
     */
    @Override
    public void getInjury(int damage) {
        this.health -= damage;
        if (this.health <= 0) {
            this.health = 0;
            this.isAlive = false;
        }
    }

    /**
     * Return the score of the player.
     *
     * @return
     */
    public int getXp() {
        return this.xp;
    }

    /**
     * Allows update the score of the player.
     *
     * @param xp
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * Return the numbers of pieces.
     *
     * @return the numbers of pieces
     */
    public int getPieces() {
        return this.pieces;
    }

    /**
     * Return the name of the player.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Allows update the numbers of pieces.
     *
     * @param pieces a numbers of pieces
     */
    public void setPieces(int pieces) {
        this.pieces = pieces;
    }

    /**
     * Return the final numbers of pieces.
     *
     * @return the final numbers of pieces
     */
    public int getFinalPieces() {
        return this.finalPieces;
    }

    /**
     * Return the final numbers of diamonds.
     *
     * @return the final numbers of diamonds
     */
    public int getFinalDiamonds() {
        return this.finalDiamonds;
    }

    /**
     * Authorization for the player becomes invisible
     *
     * @return true the player is authorized,false the player is not authorized
     */
    public boolean isAuthorizedInv() {
        return authorizedInv;
    }

    /**
     * Allows update the authorization for the player becomes invisible
     *
     * @param authorizedInv a boolean for the authorization
     */
    public void setAuthorizedInv(boolean authorizedInv) {
        this.authorizedInv = authorizedInv;
    }

    /**
     * Allows update the numbers of pieces. Add one piece.
     */
    public void addPieces() {
        this.pieces++;
        this.finalPieces += this.pieces;
    }

    /**
     * Returns the weapon that is used.
     *
     * @return the weapon
     */
    public Weapons getWeaponUsed() {
        return this.weaponUsed;
    }

    /**
     * Allows update the weapon that is used.
     *
     * @param wUse a weapon
     */
    public void setWeaponUsed(Weapons wUse) {
        this.weaponUsed = wUse;
    }

    /**
     * Allows update the name of the player.
     *
     * @param name a name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Increases the player's lives
     *
     * @param health the lives
     * @throws MoneyOrDeathException
     */
    public void addHealth(int health) throws MoneyOrDeathException {
        if (this.health + health <= this.healthMax) {
            setHealth(this.health + health);
        } else {
            setHealth(this.healthMax);
        }
    }

    /**
     * Increase the number of diamonds.
     *
     * @param nbDiamonds a number of diamonds
     */
    public void addDiamonds(int nbDiamonds) {
        this.nbOfDiamond += nbDiamonds;
    }

    /**
     * Validates the number of diamonds.
     */
    public void validDiamonds() {
        this.finalDiamonds += this.nbOfDiamond;
    }

    /**
     * Reset the state of the player.
     */
    public void resetStat() {
        this.pieces = 0;
        this.nbOfDiamond = 0;
        this.health = this.healthMax;
        this.isAlive = true;
    }

    /**
     * Attacks an enemy.
     *
     * @param c a character
     */
    @Override
    public void attack(Character c) {
        synchronized (this) {
            int damage;
            //int nbDiamond;
            Enemy e = (Enemy) c;
            damage = this.weaponUsed.getDamage();
            e.getInjury(damage);
            if (e.getIsLive()) {
                setXp(getXp() + e.getPower());
            } else {
                //nbDiamond = getNbOfDiamond();
                addDiamonds(e.getNbOfDiamond());
                //setNbOfDiamond(nbDiamond + e.getNbOfDiamond());
                e.setNbOfDiamond(0);
            }

        }
    }

    /**
     * Allow update of initial player position
     *
     * @param pos The new position
     */
    public void setInitPosition(Position pos) {
        initPos = new Position(pos);
    }

    /**
     * Reset the initial position for the player
     */
    public void resetPosition() {
        setPosGrid(new Position(initPos));
    }
}
