package be.he2b.moneyordeath.model;

/**
 * Represent weapons use by the player. With some damage etc
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class Weapons {
    private TypeWeapons type ;
    private int damage ; 
    
    /**
     * Create a weapon
     * @param typeWeapon Her type
     * @param damage  Her damage
     */
    public Weapons(TypeWeapons typeWeapon, int damage) {
        this.type = typeWeapon ; 
        this.damage = damage ;
    }
    
    /**
     * Return the type weapon
     * @return the type
     */
    public TypeWeapons getTypeWeapons() {
        return this.type ;
    }
    
    /**
     * Return the damage this weapon
     * @return  the damage
     */
    public int getDamage() {
        return this.damage ;
    }
    
    /**
     * Allows update damage this weapon
     * @param damage the new damage
     * @throws MoneyOrDeathException 
     */
    public void setDamage(int damage) throws MoneyOrDeathException {
        if(damage<0)
            throw new MoneyOrDeathException("Damage negatif");
        
        this.damage = damage ; 
    }
}
