package be.he2b.moneyordeath.model;

import java.io.Serializable;

/**
 * Represent a component in the world game.
 * @author Ordoñez Jonathan
 * @author Antar Gaby
 * @author Madrane Sofiane
 */
public class ElementArea implements Serializable{

    private ElementAreaType type;
    private Position posGrid;

    /**
     * Creates a element of the area
     *
     * @param type type of the element
     * @param posGrid position of the element
     */
    public ElementArea(ElementAreaType type, Position posGrid) {
        this.type = type;
        this.posGrid = posGrid;
    }
    
    /**
     * Copy constructor a element of the area
     * @param ea  a element of the area
     */
    public ElementArea(ElementArea ea) {
        this.type = ea.type;
        this.posGrid = ea.posGrid;
    }
    
    /**
     * Return the type of the element
     *
     * @return the type
     */
    public ElementAreaType getType() {
        return type;
    }
    
    /**
     * Return the position of the element
     *
     * @return
     */
    public Position getPosGrid() {
        return posGrid;
    }
    
    /**
     * Allows update the position
     *
     * @param posGrid position of the element
     */
    public void setPosGrid(Position posGrid) {
        this.posGrid = posGrid;
    }
    
    /**
     * Override method toString for element of the area
     * @return represents a element of the area in string
     */
    @Override
    public String toString() {
        String chaine;
        chaine = "(" + posGrid.getRow() + "," + posGrid.getColumn() + ")";
        return chaine;
    }
}