/*
 * Represents the game grid
 */
package be.he2b.moneyordeath.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent the grid game
 * @author Madrane Sofiane
 * @author Ordoñez Jonathan
 * @author Antar Gaby
 */
public class Grid {

    private final static int COL = 30, ROW = 12;
    private ElementArea[][] tabElementArea;
    private Player player;
    private List<Enemy> listEnemy;
    private int numberOfpiece;
    private int numberOfDiamond;

    /**
     * Create a grid
     *
     * @param player the player
     * @param listEnemy the list of enemy
     * @param tabElementArea the grid of the elements
     * @param numberOfpiece number of the pieces
     * @param numberOfDiamond number of the diamond
     */
    public Grid(Player player, List<Enemy> listEnemy, ElementArea[][] tabElementArea, int numberOfpiece,
            int numberOfDiamond) {
        this.player = player;
        this.listEnemy = listEnemy;
        this.tabElementArea = tabElementArea;
        this.numberOfpiece = numberOfpiece;
        this.numberOfDiamond = numberOfDiamond;
    }
    
    /**
     * Create a grid
     * (Copy constructor)
     * @param level 
     * @throws MoneyOrDeathException 
     */
    public Grid(Grid level) throws MoneyOrDeathException {
        this.player = new Player(level.player);
        this.numberOfDiamond = level.numberOfDiamond;
        this.numberOfpiece = level.numberOfpiece;
        
        listEnemy = new ArrayList<>();
        
        for(int i = 0; i<level.listEnemy.size();i++)
            this.listEnemy.add(new Enemy(level.listEnemy.get(i)));
        
        
        this.tabElementArea = new ElementArea [Grid.ROW] [Grid.COL];
        
        for(int i =0; i<level.tabElementArea.length; i++) {
            for(int j = 0; j<level.tabElementArea[i].length; j++) {
                this.tabElementArea[i][j] = new ElementArea(level.tabElementArea[i][j]);
            }
        }
        
    }

    /**
     * Return the column of the grid
     *
     * @return the column
     */
    public static int getCol() {
        return COL;
    }

    /**
     * Return the ROW of the grid
     *
     * @return the ROW
     */
    public static int getRow() {
        return ROW;
    }

    /**
     * Return the number of pieces
     *
     * @return the number of pieces
     */
    public int getNumberOfpiece() {
        return numberOfpiece;
    }

    /**
     * Return the number of the diamond
     *
     * @return the number
     */
    public int getNumberOfDiamand() {
        return numberOfDiamond;
    }

    /**
     * Allows update the number of pieces
     *
     * @param numberOfpiece a number of pieces
     */
    public void setNumberOfpiece(int numberOfpiece) {
        this.numberOfpiece = numberOfpiece;
    }

    /**
     * Allows update the number of diamond
     *
     * @param numberOfDiamond a number of pieces
     */
    public void setNumberOfDiamand(int numberOfDiamond) {
        this.numberOfDiamond = numberOfDiamond;
    }

    /**
     * Allows to check if the position is in the grid
     *
     * @param position a position
     * @return true if is into the grid , false if is not into the grid
     */
    public boolean contains(Position position) {
        return ((position.getColumn() >= 0 && position.getColumn() < COL)
                && (position.getRow() >= 0 && position.getRow() < ROW));
    }

    /**
     * Return the element of the grid
     *
     * @param position the position of the element
     * @return the element
     */
    public ElementArea getElementArea(Position position) {
        if (!contains(position)) {
            return null;
        }
        return tabElementArea[position.getRow()][position.getColumn()];
    }

    /**
     * Allows update a element of a position
     *
     * @param position a position
     * @param element the new element
     */
    public void setElementArea(Position position, ElementArea element) {
        tabElementArea[position.getRow()][position.getColumn()] = element;//  tabElementArea[position.getRow()][position.getColumn()] = element;
    }

    /**
     * Return the grid of elements
     *
     * @return the grid
     */
    public ElementArea[][] getTabElementArea() {
        return tabElementArea;
    }

    /**
     * Allows update the element of the grid
     *
     * @param tabElementArea a element
     */
    public void setTabElementArea(ElementArea[][] tabElementArea) {
        this.tabElementArea = tabElementArea;
    }

    /**
     * Return the player
     *
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Allows update the player
     *
     * @param player a player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Return the list of enemy
     *
     * @return the list
     */
    public List<Enemy> getListEnemy() {
        return listEnemy;
    }

    /**
     * Allows update the list of enemy
     *
     * @param listEnemy a list of enemy
     */
    public void setListEnemy(List<Enemy> listEnemy) {
        this.listEnemy = listEnemy;
    }
}
