package be.he2b.moneyordeath.model;

/**
 * Represent the differents weapons in the game
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public enum TypeWeapons {
    PUNCH,
    KICK;
}
