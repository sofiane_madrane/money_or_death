package be.he2b.moneyordeath.model;

/**
 * Represent an ennemy in the game (zombie)
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class Enemy extends Character {

    private int power = 5;

    /**
     * Create an enemy
     *
     * @param ea the type of the enemy
     * @param initPosition the position of the enemy
     * @param direction the direction of the enemy
     * @param heatlh the heatlh of the enemy
     * @throws be.he2b.moneyordeath.model.MoneyOrDeathException
     */
    public Enemy(ElementAreaType ea, Position initPosition, Direction direction, int heatlh) throws MoneyOrDeathException {
        super(heatlh, ea, initPosition);
        this.nbOfDiamond = 0;
        this.direction = direction;
        chooseSense(ea);
        this.isAlive = true;
    }

    /**
     * Creates an enemy
     *
     * @param ea the type of the enemy
     * @param initPosition the position of the enemy
     * @param direction the direction of the enemy
     * @throws MoneyOrDeathException
     */
    public Enemy(ElementAreaType ea, Position initPosition, Direction direction) throws MoneyOrDeathException {
        this(ea, initPosition, direction, 100);
    }

    /**
     * Creates an enemy
     * Copy constructor
     *
     * @param enemy a enemy
     * @throws MoneyOrDeathException
     */
    public Enemy(Enemy enemy) throws MoneyOrDeathException {

        super(enemy.getHealth(), enemy.getType(), enemy.getPosGrid());
        this.nbOfDiamond = enemy.getNbOfDiamond();
        this.direction = enemy.getDirection();
        chooseSense(enemy.getType());
        this.isAlive = true;
    }

    private void chooseSense(ElementAreaType ea) {
        if (ea == ElementAreaType.ENEMYRIGHT) {
            this.sense = Direction.RIGHT;
        } else {
            this.sense = Direction.LEFT;
        }
    }

    /**
     * Returns the power of the enemy
     *
     * @return the power of the enemy
     */
    public int getPower() {
        return this.power;
    }

    /**
     * Allows update the power of the enemy
     *
     * @param power a power
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * Allows update the health of the player
     *
     * @param damage the number of lives decreased
     */
    @Override
    public void getInjury(int damage) {
        this.health -= damage;
        if (this.health <= 0) {
            this.health = 0;
            this.isAlive = false;
        }
    }

    /**
     * Attacks an enemy
     *
     * @param c a character
     */
    @Override
    public void attack(Character c) {
        synchronized (this) {
            ((Player) c).getInjury(power);
        }
    }
}
