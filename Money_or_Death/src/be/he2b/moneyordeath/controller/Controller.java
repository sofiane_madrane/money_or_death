package be.he2b.moneyordeath.controller;

import be.he2b.moneyordeath.model.Direction;
import be.he2b.moneyordeath.model.Game;
import be.he2b.moneyordeath.model.ModFacade;
import be.he2b.moneyordeath.model.MoneyOrDeathException;
import be.he2b.moneyordeath.model.TypeWeapons;
import be.he2b.moneyordeath.model.Weapons;
import be.he2b.moneyordeath.view.AttackAnimation;
import be.he2b.moneyordeath.view.Manager;
import be.he2b.moneyordeath.view.PlayerView;
import java.net.URL;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Timeline;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * The controller of the game.
 *
 * @author Antar Gaby
 * @author Madrane Sofiane
 * @author Ordonez Jonathan
 */
public class Controller {

    private final Pane root;
    private final PlayerView playerBean;
    private final Timeline timerInvisible;
    private final Manager manager;
    private final MediaPlayer soundGame;
    private final Game game;
    private AttackAnimation animate;
    private MediaPlayer soundKick, soundPunch;

    /**
     * Create the controller of the game.
     *
     * @param model a game
     * @param root a pane
     * @param playerBean the player bean
     * @param timerInvisible a timer
     */
    public Controller(Game model, Pane root, PlayerView playerBean, Timeline timerInvisible, Manager manager,MediaPlayer soundGame) {
        this.game = model;
        this.root = root;
        this.playerBean = playerBean;
        this.timerInvisible = timerInvisible;
        this.manager = manager;
        this.soundGame = soundGame;
        initMotionListeners();
        initSound();
    }

    private void initSound() {
        URL ressourcePunch = getClass().getResource("/sound/punch.wav");
        URL ressourceKick = getClass().getResource("/sound/kick.wav");
        Media mediaPunch = new Media(ressourcePunch.toString());
        Media mediaKick = new Media(ressourceKick.toString());
        soundKick = new MediaPlayer(mediaKick);
        soundPunch = new MediaPlayer(mediaPunch);
    }
    
    /**
     * Reset the game.
     *
     * @throws MoneyOrDeathException
     */
    public void resetGame() throws MoneyOrDeathException {
        game.resetGame();
    }
    
    /**
     * Setter the value Coin collected of game
     *
     * @param value New Value
     */
    public void setCoinCollected(boolean value){
        game.setCoinCollected(value);
    }

    /**
     * Subscribe a player in database.
     *
     * @param name The name player
     * @throws MoneyOrDeathException
     */
    public void subscribeDB(String name) throws MoneyOrDeathException {
        ModFacade db = game.getDataBase();
        game.getCurrentPlayer().setName(name);
        if (game.getDataBase().existPlayer(game.getCurrentPlayer().getName())) {
            db.updatePlayer(game.getCurrentPlayer());
        } else {
            db.insertPlayer(game.getCurrentPlayer());
        }
    }

    /**
     * Displays tuples of the database
     *
     * @return the tuples
     * @throws MoneyOrDeathException
     */
    public String readRow() throws MoneyOrDeathException {
        ModFacade db = game.getDataBase();
        return db.readRow();
    }

    /**
     * Close the connection of the database.
     *
     * @throws MoneyOrDeathException
     */
    public void closeConnectionDB() throws MoneyOrDeathException {
        game.getDataBase().closeConnection();
    }

    private void initMotionListeners() {
        root.requestFocus();
        initOnKeyPressed();
        initOnKeyReleased();
    }

    private void initOnKeyPressed() {
        root.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case LEFT:
                    game.play(Direction.LEFT);
                    playerBean.getIv().setScaleX(-1.0); // change la direction de l'image
                    ;
                    break;
                case RIGHT:
                    game.play(Direction.RIGHT);
                    playerBean.getIv().setScaleX(+1.0);
                    ;
                    break;
                case UP:
                    game.play(Direction.UP);
                    ;
                    break;
                case DOWN:
                    game.play(Direction.DOWN);
                    ;
                    break;
                case SPACE:
                    if (game.getCurrentPlayer().isAuthorizedInv()) {
                        game.getCurrentPlayer().setInvisible(true);
                        playerBean.setStateInvisible(true);
                        timerInvisible.play();
                    }
                    break;
                case ESCAPE:
                    confirmationGameDialog();
                    break;
                case Q:  // Les touches virtuelles en qwerty donc Q->A
                    game.getCurrentPlayer().setAttack(true);
                    game.playerAttack(new Weapons(TypeWeapons.PUNCH, 10));
                    this.animate = new AttackAnimation(playerBean, game.getCurrentPlayer());

                    animate.start();
                    soundPunch.play();
                    break;
                case W:  // Les touches virtuelles en qwerty donc W->Z
                    game.getCurrentPlayer().setAttack(true);
                    game.playerAttack(new Weapons(TypeWeapons.KICK, 25));
                    this.animate = new AttackAnimation(playerBean, game.getCurrentPlayer());

                    animate.start();
                    soundKick.play();
                    break;
            }
        }
        );
    }

    private void initOnKeyReleased() {
        int ind = 15 % playerBean.getImgsRunning().length;
        root.setOnKeyReleased(e -> {
            switch (e.getCode()) {
                case LEFT:
                    playerBean.getIv().setImage(playerBean.getImgsRunning()[ind]);
                    break;
                case RIGHT:
                    playerBean.getIv().setImage(playerBean.getImgsRunning()[ind]);
                    break;
                case Q:
                    soundPunch.seek(Duration.ZERO);
                    break;
                case W:
                    soundKick.seek(Duration.ZERO);
                    break;
            }
        });
    }

    private void confirmationGameDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Money or death");
        alert.setHeaderText("Voulez-vous fermer la partie ? ");
        alert.setContentText("Choisissez une option... ");

        ButtonType buttonTypeYes = new ButtonType("Oui");
        ButtonType buttonTypeNo = new ButtonType("Non");

        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);

        Optional<ButtonType> result = alert.showAndWait();

        try {
            if (result.get() == buttonTypeYes) {
                closeConnectionDB();
                game.stopThreadEnemy();
                game.stopThreadFallPlayer();
                soundGame.stop();
                this.manager.setScene_Start();
            }
        } catch (MoneyOrDeathException ex) {
        }
    }
}
