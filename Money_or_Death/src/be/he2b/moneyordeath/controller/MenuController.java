package be.he2b.moneyordeath.controller;

import be.he2b.moneyordeath.view.Manager;
import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.shape.Path;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * The main menu controller of the game. FXML Controller class for the main scene.
 *
 * @author Jonathan
 * @author Antar Gaby
 * @author Madrane Sofiane
 */
public class MenuController{

    @FXML
    private Button btnChooseWorld;
    
    private Manager manager;

    /**
     * Initializes the controller class.
     * @param manager The main stage.
     */
    public void initialize(Manager manager){
        this.manager = manager;
    }
    
    /**
     * Choose the file with the levels to play.
     */
    @FXML
    public void ChooseWorldClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(
            new File("./resources/levels"));
        
        fileChooser.getExtensionFilters().addAll(
         new ExtensionFilter("Text Files", "*.txt"));
        
        File selectedFile = fileChooser.showOpenDialog(btnChooseWorld.getScene().getWindow());
        if (selectedFile != null) {
            manager.setScene_Game(selectedFile);
        }
    }
    
    /**
     * Switch to the MapEditor
     */
    @FXML
    public void CreateWorldClicked() {
        manager.setScene_Generator();
    }
}
