package be.he2b.moneyordeath.controller;

import be.he2b.moneyordeath.view.generator.*;
import be.he2b.moneyordeath.model.ElementAreaType;
import be.he2b.moneyordeath.view.Manager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.stage.FileChooser;

/**
 * FXML Controller class for the map editor.
 *
 * @author Jonathan
 */
public class MapEditorController {

    @FXML
    private TilePane grid;
    @FXML
    private TilePane gridButtons;
    @FXML
    private Button cancelButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button saveAsButton;
    
    private ElementAreaType currentType;
    private boolean dragActive;
    private int rows;
    private int columns;
    private Manager manager;
    private File currentFile;

    /**
     * Initializes the controller class.
     * @param manager The main stage
     * @param rows Number of rows for the MapEditor
     * @param columns Number of columns for the MapEditor
     */
    public void initialize(Manager manager, int rows, int columns) {
        this.manager = manager;
        this.rows = rows;
        this.columns = columns;
        this.currentFile = null;
                
        currentType = ElementAreaType.EMPTY;
    
        grid.setPrefColumns(this.columns);
        grid.setPrefRows(this.rows);
        
        grid.setStyle("-fx-border-color: black;");
        
        for(int j=0;j< (this.columns * this.rows);j++){    
            Cell cell = new Cell();
                
            cell.setOnMousePressed((MouseEvent t) -> {
                if( t.isPrimaryButtonDown()) {
                    if(!isPlayerPresent() || currentType != ElementAreaType.PLAYER){
                        cell.setValue(currentType);
                    }
                } else if( t.isSecondaryButtonDown()) {
                    cell.setValue(ElementAreaType.EMPTY);
                }
            });
                
            cell.setOnMouseDragEntered((MouseEvent t) -> {
                if( t.isPrimaryButtonDown()) {
                    if(dragActive){
                    cell.setValue(currentType);
                    }
                } else if( t.isSecondaryButtonDown()) {
                    cell.setValue(ElementAreaType.EMPTY);
                }
            });
            
            grid.getChildren().add(cell);
        }
        
        for(int i=0;i<6;i++){  
            ChoseButton button = new ChoseButton();
            button.setOnAction((event) -> {
                currentType = button.getType();
                dragActive = button.getType() != ElementAreaType.PLAYER &&
                             button.getType() != ElementAreaType.ENEMYLEFT;
            });
            
            gridButtons.getChildren().add(button);
        }
        
        /*saveButton.disableProperty().bind(new BooleanBinding() {
                {
                    bind(cell.valueProperty());
                }

                @Override
                protected boolean computeValue() {
                    return cell.getValue() == ElementAreaType.PLAYER;
                }
            });*/
        
        cancelButton.requestFocus();
    }    
    
    /**
     * Delete all current grid elements.
     */
    @FXML
    public void eraseAll(){
        for (Iterator<Node> it = grid.getChildren().iterator(); it.hasNext();) {
            Cell n = (Cell) it.next();
            n.setValue(ElementAreaType.EMPTY);
        }  
    }
    
    /**
     * Cancel and return to main menu.
     */
    @FXML
    public void cancel(){
        
        manager.setScene_Start();
    }
    
    /**
     * Save the current grid in the same File.
     */
    @FXML
    public void save(){
        if(currentFile == null){
            saveAs();
        }else{
            SaveFile(getLevelString(), currentFile);
            messageFileSaved();
        }     
    }
    
    /**
     * Save the current grid in a new File.
     */
    @FXML
    public void saveAs(){ 
        showFileChooser();
        if(currentFile != null){
            SaveFile(getLevelString(), currentFile);
            messageFileSaved();
        }
    }
    
    private void messageFileSaved(){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Saved");
        alert.setHeaderText(null);
        alert.setContentText("The file " + currentFile.getName() + " has been saved !");
        alert.showAndWait();
    }
    
    private void showFileChooser(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(
            new File("./resources/levels"));
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);       
        //Show save file dialog
        currentFile = fileChooser.showSaveDialog(saveAsButton.getScene().getWindow());
    }
    
    private String getLevelString() {  
        String level;
        Iterator<Node> it = grid.getChildren().iterator();
        it.hasNext();
        //sauvegarder le contenu de la Map dans un fichier.
        level = "==============================\r\n";
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                Cell n = (Cell) it.next();
                switch(n.getValue()){
                    case EMPTY:
                        level += " ";
                        break;
                    case BRICK:
                        level += "-";
                        break;
                    case PIECE:
                        level += "P";
                        break;
                    case DIAMAND:
                        level += "D";
                        break;
                    case LADDER:
                        level += "H";
                        break;
                    case PLAYER:
                        level += "J";
                        break;    
                    case ENEMYLEFT:
                        level += "L";
                        break;
                }
            }
            level += "\r\n";
        }
        level += "==============================\r\n";
        return level;
    }
    
    private boolean isPlayerPresent(){
        boolean res = false;
        for (Iterator<Node> it = grid.getChildren().iterator(); it.hasNext();) {
            Cell n = (Cell) it.next();
            res = n.getValue() == ElementAreaType.PLAYER;
            if(res){
                return res;
            }
        }
        return res;
    }
    
    /*private boolean areElementsPresentstoSave(){
        boolean res = false;
        int count = 0;
        for (Iterator<Node> it = grid.getChildren().iterator(); it.hasNext();) {
            Cell n = (Cell) it.next();
            res = (n.getValue() == ElementAreaType.PLAYER) ? :;
            if(res){
                return res;
            }
        }
        return res;
    }*/
    
    private void SaveFile(String content, File file){
        try {
            FileWriter fileWriter = null;
             
            fileWriter = new FileWriter(file,true);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            //Logger.getLogger(JavaFX_Text.class.getName()).log(Level.SEVERE, null, ex);
        }     
    }
    
}