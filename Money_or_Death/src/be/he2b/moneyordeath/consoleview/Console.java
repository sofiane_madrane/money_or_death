package be.he2b.moneyordeath.consoleview;

import be.he2b.moneyordeath.model.MoneyOrDeathException;
import be.he2b.moneyordeath.model.Direction;
import be.he2b.moneyordeath.model.TypeWeapons;
import be.he2b.moneyordeath.model.Weapons;
import java.util.Scanner;

/**
 * The console.
 * @author Antar Gaby
 */
public class Console {

    /**
     * Allows to play with the console mode
     *
     * @param args
     * @throws be.he2b.moneyordeath.model.MoneyOrDeathException
     */
    public static void main(String[] args) throws MoneyOrDeathException {
        char d;
        GameView view = new GameView();
        view.getJeux().startThreadEnemy();
        Scanner clavier = new Scanner(System.in);
        while (!view.getJeux().getIsOver()) {
            System.out.println(" Chose a movement : ");
            System.out.print("down(d), up(u), right(r),left(l) :");
            d = clavier.next().charAt(0);
            while (d != 'd' && d != 'r' && d != 'u' && d != 'l' && d != 'z') {
                System.out.println("Wrong direction, try again");
                System.out.print("down(d), up(u), right(r),left(l) :");
                d = clavier.next().charAt(0);
            }
            if (dep(d) == null) {
                view.getJeux().playerAttack(new Weapons(TypeWeapons.PUNCH, 25));
            } else {
                view.getJeux().play(dep(d));
            }
        }
    }

    /**
     * Allows to replace a character per a moving
     *
     * @param ch a character
     * @return the direction
     */
    public static Direction dep(char ch) {
        switch (ch) {
            case 'u':
                return Direction.UP;

            case 'r':
                return Direction.RIGHT;

            case 'l':
                return Direction.LEFT;

            case 'd':
                return Direction.DOWN;
            default:
                return null;

        }

    }
}
