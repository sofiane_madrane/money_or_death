package be.he2b.moneyordeath.consoleview;

import be.he2b.moneyordeath.model.ElementArea;
import be.he2b.moneyordeath.model.ElementAreaType;
import be.he2b.moneyordeath.model.Enemy;
import be.he2b.moneyordeath.model.Game;
import be.he2b.moneyordeath.model.MoneyOrDeathException;
import be.he2b.moneyordeath.model.Position;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * The game view.
 *
 * @author Antar Gaby
 */
public class GameView implements Observer {

    private Game jeux;

    @Override
    public void update(Observable o, Object o1) {
        if (o instanceof Game) {
            System.out.println(display());
        }
    }

    /**
     * Creates the gameview
     *
     * @throws MoneyOrDeathException
     */
    public GameView() throws MoneyOrDeathException {
        jeux = new Game();
        System.out.println(this.jeux);
        System.out.println(display()); // display the game
        jeux.addObserver(GameView.this);
    }

    private String display() {
        String chaine = "";
        ElementArea curElement;
        Position curPosPlayer;
        for (int i = 0; i < 12; i++) {
            chaine = chaine + " \n";
            for (int j = 0; j < 30; j++) {
                curElement = jeux.getLevel().getTabElementArea()[i][j];
                if (curElement != null) {
                    String var = type(curElement);
                    curPosPlayer = jeux.getCurrentPlayer().getPosGrid();
                    if (curPosPlayer.getRow() == i && curPosPlayer.getColumn() == j) {
                        var = "J";
                    } else {
                        List<Enemy> curListEnemy = jeux.getLevel().getListEnemy();
                        for (int k = 0; k < curListEnemy.size(); k++) {
                            Enemy curEnemy = jeux.getLevel().getListEnemy().get(k);
                            if (curEnemy.getPosGrid().getRow() == i
                                    && curEnemy.getPosGrid().getColumn() == j) {
                                if (curEnemy.getType() == ElementAreaType.ENEMYRIGHT) {
                                    var = "R";
                                } else {
                                    var = "L";
                                }
                            }
                        }
                    }
                    chaine = chaine + var;
                }

            }
        }
        return chaine;
    }

    /**
     * Replaces an element of the grid per a character
     *
     * @param element a element
     * @return the character
     */
    public String type(ElementArea element) {
        switch (element.getType()) {
            case PLAYER:
                return "J";

            case BRICK:
                return "-";

            case ENEMYLEFT:
                return "L";

            case ENEMYRIGHT:
                return "R";

            case LADDER:
                return "H";

            case PIECE:
                return "P";

            case EMPTY:
                return " ";

        }
        return (" err ");
    }

    /**
     * Return the game
     *
     * @return the game
     */
    public Game getJeux() {
        return jeux;
    }

    /**
     * Allows update the game.
     * @param jeux a game
     */
    public void setJeux(Game jeux) {
        this.jeux = jeux;
    }
}
